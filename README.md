[TOC]

# HGTracker Overview

The HGTracker pipeline enables the identification of interkingdom Horizontally Gene Transfers (HGT) in metazoan genomes. It uses the blast results of all proteins in a metazoan genome against the NCBI's non-redundant protein database (nr) to infer the taxonomic origin of each protein. It then links this taxonomic information and the location of the proteins in the genome to classify all proteins as Native, Unclassifiable or Foreign, and if Foreign genes are horizontally transferred genes (HGT), are in a completely foreign scaffold (FS) or are in ambiguous scaffold with foreign and unclassifiable genes.

# System requirements 

* 70+ GB of free memory, mostly to host the NCBI databases.

* Working versions of the programs make, gcc and unzip.

* A version of [Perl 5](http://www.perl.org/get.html)

* [Python 2.7.3 or 2.7.6](http://www.python.org/download/releases/2.7.6/)

And the following toolkits:

* [bedtools](http://bedtools.readthedocs.org/en/latest/content/installation.html)

* [blast+](http://www.ncbi.nlm.nih.gov/books/NBK1763/#_CmdLineAppsManual_Installation_)

Note: The HGTracker pipeline currently works in Linux and Mac computers. You should be able to use this pipeline in a Windows machine using a linux-like environment for Windows such as [Cygwin](http://www.cygwin.com/), although this setup is currently untested.

# Installation

Download the following binaries in from the [kent source](http://hgdownload.cse.ucsc.edu/admin/exe/).

Select your operating system:

* linux.x86\_64.v287        
* linux.x86\_64             
* macOSX.i386              
* macOSX.ppc               
* macOSX.x86\_64 

Download the necessary tools by clicking on the links and putting them in an executable directory. 

* **overlapSelect**
* **faSomeRecords**
* **faToTwoBit**
* **twoBitInfo**
* **faSize**

Note: The binaries for the linux.x86\_64 are provided with HGTracker in the folder "utils\_x86\_64". The above listed tools were developed by the UCSC genome browser team (more information [here](http://genome.ucsc.edu/license/)).

Once all prerequistes are installed use the start up script:

	./HGTracker_Installer.sh

This script will download and start up the following NCBI databases, as well as packages required from BioPerl:

* Non Redundant database (nr) 
* Taxonomy 
* gi to tax dump

This might take a few hours. 

If you are installing HGTracker on a server or a shared computer you can send the process to the background so it continues downloading even if you log out. Note: This will not work if the computer/server is taken off the network or if it's turned off. 

	nohup ./HGTracker_Installer.sh &

__Important! Please download both the nr and taxonomy databases at the same time or at the very least the same day and only blast against these, else you will have missing taxonomic annotations for some sequences.__

Finally export the bin directory blast database by adding the following line to your .bash\_profile or .bashrc. Change the pipeline install directory accordingly:

	export BLASTDB=$BLASTDB:/your/pipeline/install/directory/nr_database
	
	export PATH=$PATH:/your/pipeline/install/directory/bin

Then restart your terminal session. If this step worked you should be able see the help of HGTracker.pl outside of the install directory.

	HGTracker.pl

# Required files

To use HGTracker you need the following files:

* Protein Sequences - Protein sequences of all gene model in the organism of interest in fasta format.
* Gene Models - Location of all gene models in the organism of interest in [bed 12 format](http://genome.ucsc.edu/FAQ/FAQformat.html#format1). 
* mRNA Sequences - mRNAs of proteins of interest in fasta format (cDNA).
* Genome - Genome sequences in fasta format. This file should include gaps in the genome assembly denoted by N's and each scaffold should be included as a separate record

All files need to share the same unique identifiers for each protein of interest as well as for all scaffolds so they can be linked back between the different programs. If a gene has several isoforms it is better to put all protein isoforms independently to avoid exclusion of information. 

To get an idea on how these files should look like check the example files provided with the pipeline in the directory "sample\_data". Sample blast results are not provided, as they will change as the database changes.
 
* sample\_proteins.fa
* sample\_coordinates.bed
* sample\_mRNA.fa
* sample\_genome.fa

# For the impatient 

You need to retrieve the taxonomic number for the genome you are analysing from [NCBI Taxonomy](http://www.ncbi.nlm.nih.gov/taxonomy). The taxonomy ID for the sample data provided with the pipeline is 400682.

If installation has been successful the following command will carry out the complete analysis, including the blast search (on a single core):

	HGTracker.pl -p sample_proteins.fa -b sample_coordinates.bed -c sample_mRNA.fa -g sample_genome.fa -t 400682

If you have already generated your blast results you can provide them using:

	HGTracker.pl -p sample_proteins.fa -b sample_coordinates.bed -c sample_mRNA.fa -g sample_genome.fa -r results_sample_proteins.blast -t 400682

# HGTracker.pl  

### Description

The HGTracker.pl script is a wrapper that executes the complete HGTracker analysis pipeline sequentially. It can carry out the blast step although it is recommended to do this independently using a parallelized blast setup (see blast section below) to significantly speed up the analysis. 

HGTracker taxonomically classifies all blast hits and uses these classifications to identify native, foreign and genes that cannot be classified. It finally compiles the gene composition of each genome scaffold and uses it to classify Foreign genes as hosted in a scaffold that might be HGT, that has only foreign genes, that is Ambiguous or Unclassifiable. See the output section for a more detailed description on what each classification means.

### Example 
	
	HGTracker.pl -p sample_proteins.fa -b sample_coordinates.bed -c sample_mRNA.fa -g sample_genome.fa -r results_sample_proteins.blast -t 400682

### Parameters 

Mandatory parameters are marked with an asterisk (\*).

	*[--proteins  | -p ] - Protein file in fasta format
	*[--bedfile   | -b ] - Protein coordinates in genome of interest (bed format)
	*[--cDNA      | -c ] - mRNA sequences in fasta format (cDNA)
	*[--genome    | -g ] - Genome of interest in fasta format
	 [--blast     | -r ] - Blast results for protein file [optional]

### Output files 

All output files are described each output file section of the step-by-step analysis. The primary output with the final gene classifications is [Classification_Summary_Final_classification_sample_proteins.txt](https://bitbucket.org/lizfernandez/hgtracker/overview#markdown-header-output-files_4).

# Step by step analysis 

This is a detailed description of all the steps carried out automatically by HGTracker.pl in the same order as described. All example commands shown can be executed using the example data provided with the pipeline as long as the previous step has been successfully executed.

## 0. Blasting your peptide sequences

HGTracker uses blastp to search for similar sequences in the NCBI's non-redundant protein database or nr. The most recent available version of the nr database is automatically downloaded upon [installation](https://bitbucket.org/lizfernandez/hgtracker#markdown-header-install). Make sure you copy the database and export the blast database variable (BLASTDB) if you are running the blast search step on a different computer/server. 

### Single process blast

This is only recommended if you have very limited computational resources (e.g. a single machine with few computing cores). The num\_threads parameter must be adjusting according to the number of cores available.

	blastp -query sample_proteins.fa -evalue 0.0001 -max_target_seqs 200 -db nr -num_threads 8 -outfmt '7 qseqid qlen sseqid pident length mismatch gapopen qstart qend sstart send ppos evalue bitscore score' -out results_sample_proteins.blast

Note: Running a single blastp command will take a long time for most genomes, carrying out the search in parallel using several blast processes in different CPUs will significantly reduce the running time. If you have access to a server you should use the generate\_blast\_commands.sh script (described below) to generate separate blast commands that can be executed in parallel. This is highly recommended.

### Parallel blast setup

To speed up your blast analysis you want to carry out your blast searches in parallel. You can easily generate hundreds of blast commands and their inputs using the generate\_blast\_commands.sh script.

The script outputs a list of the blast commands that need to be carried out before doing the automated taxonomic classification step. It splits up the fasta input in order for the commands to be run in parallel using swarm, job arrays or similar. Contact your server administrator(s) for advice.

__Example__

	generate_blast_commands.sh -f sample_proteins.fa -n 100 -t 4 -p /home/user/analysis_directory/

__Parameters__

Mandatory parameters are marked with an asterisk (\*).

	*[ -f ] - Input fasta file with proteins to be blasted 
	*[ -n ] - Number of sequences in each file in which the fasta will be split
	*[ -p ] - Path of fasta and ouput file. This path should be where the blast will be carried out, even if in different computer or server.
	*[ -t ] - Number of threads per blast job (available CPUs) 

The script will divide your protein fasta file in several file, each containing the specified number of sequences in the -p parameter (for example, the command above will generate files with 100 or less sequences each). It will also generate a file called __Blast\_Commands\_Peptides.pep.fa.txt__, where each line is a single blast command that can be executed in parallel. Make sure the split peptide files are in the same directory to where you are carrying out your blast search.

If you have a single machine or CPU available you want run a single command (see previous section). 

Note: If the server where you installed the pipeline is different to the server where you carry out your blast search you want to copy the nr database located in your install directory onto the server and export the directory onto the BLASTDB variable. 

	export BLASTDB=/my/blast/database/directory 

To join your blast results together simply concatenate all your blast results. 

	cat *blast > all_results_sample_proteins.blast

## 1. Classify blast hits by taxonomy

### Description

classify\_taxonomy\_blast\_hits.pl extracts the taxonomy annotation of all blast hits and uses this information to assign a taxonomic class for each gene among the following: 

	M - Metazoa
	B - Bacteria
	F - Fungi
	A - Archaea
	P - Plants
	O - Other Eukaryotes
	V - Viruses
	U - Unknown (kingdom and superkingdom not known)

It also provides the 5 best hits for the first top 5 species (BestSpecies\_<results suffix>) and the best 5 sequences in each of the following taxonomic classes (according to the NCBI taxonomy) (Best5PerClass\_<results suffix>).

You must provide the taxonomic number for the genome of interest from [NCBI Taxonomy](http://www.ncbi.nlm.nih.gov/taxonomy).

### Example

	classify_taxonomy_blast_hits.pl -i results_sample_proteins.blast -o classified_sample_proteins.txt -t 400682 -p ../taxonomy/

### Parameters

Mandatory parameters are marked with an asterisk (\*).

	*[--input       | -i ]  - Input blast output file (MUST have descriptive comments)
	*[--output      | -o ]  - Desired output file
	*[--taxid       | -t ]  - NCBI Tax ID of organism of interest (native)
	*[--path        | -p ]  - Path to taxonomy database.
	 [--dictionary  | -d ]  - Renew taxonomy dictionary. Dictionary must be where taxonomy DB is. OPTIONAL - If installation was successful this is not necessary. 

### Output files

__1. classified\_sample\_proteins.txt__
	
	1. Model           - Protein/gene model identifier
	2. AlienIndex      - Alien index calculated as log((Best E-value for Metazoa) + e-200) - log((Best E-value for Non- Metazoa) + e-200)
	3. BestHitClass    - Taxonomic class of blast best hit (M,B,F,A,P,O,V or U)
	4. SecondBestClass - Taxonomic class of second best blast hit (M,B,F,A,P,O,V or U)
	5. UsedSeqs        - Number of sequences among blast hits after removal of hits to the species of interest and synthetic protein constructs.  
	6. TotalSpecies    - Number of different species found among blast hits
	7. TotalHits       - Total number of blast hits after removal of hits to the species of interest and synthetic protein constructs. Not equivalent to number of sequences as a single sequence can appear as two blast hits.
	
The 8 following columns are a composite of 10 fields with the following structure, where the X can be any of the taxonomic classes described above ((M[8],B[9],A[10],F[11],P[12],O[13],V[14] or U[15]; in this order):

		BX:XSeqs:XPSeqs:XSp:XPSp:BXH_GI:BXH_eval:BXH_Bsc:BXSCov:BXPCov	
	
		1. BX	    - Identifier, if a hit is found in this class it will be BX (BM, BB, BF, etc). If no blast hit was found this value will be "NA"
		2. XSeqs    - Number of sequences in class X  
		3. XPSeqs   - Percentage of sequences in class X among all blast hits
		4. XSp	    - Number of species in blast hits
		5. XPSp     - Percentage of species in blast hits
		6. BXH_GI   - NCBI gi of best blast hit in this taxonomic class
		7. BXH_eval - e-value of best blast hit in this taxonomic class
		8. BXH_Bsc  - Bit-score of best blast hit in this taxonomic class
		9. BXSCov   - Number of query sequence aminoacids covered by the best blast hit this taxonomic class. If other blast hits in addition the best blast hit are found for this sequence their positions are calculated to identify total coverage of the blast hit query.
		10. BXPCov -  Percentage of query sequence aminoacids covered by the best blast hit this taxonomic class. The total coverage for the sequence corresponding to the best blast hit is calculated (see field description for field BXSCov).

And finally column: 

	16. Native models - NCBI identifiers of any native genes (found in the species of interest) found among the blast hits 

	
__2. Best5PerClass\_classified\_sample\_proteins.txt__ - Best 5 blast hits per taxonomic class. 

__3. Best5Species\_classified\_sample\_proteins.txt__  - Best 5 species among blast hits. If a single species had multiple blast hits only the best one is shown. 

Both of these files have the same following output fields:

	1.  Gi_Target     - Gene id of blast result
	2.  Class_id      - Taxonomic ID of blast result (M,B,F,A,P,O,V or U)
	3.  Superkingdom  - Superkingdom annotation (NCBI)
	4.  Kingdom       - Kingdom annotation (NCBI)
	5.  Phylum        - Phylum annotation (NCBI)
	6.  Class         - Class annotation (NCBI)
	7.  Order         - Order annotation (NCBI)
	8.  Family        - Family annotation (NCBI)
	9.  Genus         - Genus annotation (NCBI)
	10. Species       - Species annotation (NCBI)
	11. ModelID       - Identifier of gene model query
	12. Query_len     - Query size (nts)
	13. Hit_id        - NCBIs blast hit ID 
	14. Percent_ident - Percentage of sequence identity of blast hit
	15. Alig_length   - Size of alignment between query and hit (nt)
	16. Mismatches    - Number of mismatches between query and hit (nt)
	17. Gaps          - Number of gaps between query and hit (nt)
	18. Query_start   - Start position of alignment in query sequence
	19. Query_end     - End position of alignment in query sequence
	20. Hit_start     - Start position of alignment in hit sequence
	21. Hit_end       - End position of alignment in hit sequence
	22. Percent+      - Percentage of positive-scoring matches 
	23. E-value       - Expect value
	24. Bit_score     - Blast bit score
	25. Score         - Blast hit score
	26. RefSeqID      - RefSeq ID of hit if available
	27. SeqCoverage   - Query sequence coverage by blast hit (nt)
	28. %SeqCoverage  - Percentage of sequence covered by blast hit (nt)

__4. Prot\_no\_blast\_hits\_classified\_sample\_proteins.txt__

Contains the identifiers of proteins that had no blast hits and thus were not analysed.

## 2. Enhance taxonomic annotations 

### Description

The script annotate_blast_hits.pl adds sequence information to the best blast hits, best blast species and taxonomic classifications of gene models generated using classify_taxonomy_blast_hits.pl.

### Example

	annotate_blast_hits.pl -i classified_sample_proteins.txt -f sample_mRNA.fa -o complete_classified_sample_proteins.txt

### Parameters

Mandatory parameters are marked with an asterisk (\*).

	*[--input      | -i ]  - Output from classify_taxonomy_blast_hits
	*[--fasta      | -f ]  - Fasta file containing the cDNA of classified sequences
	*[--output     | -o ]  - Enhanced results including %GC and gene annotations

### Output files

__1. complete\_classified\_sample\_proteins.txt__

Same as output file 1 in previous section with the addition of two columns: 

	17. ModelSize - Size of gene model in nts
	18. ModelGC   - GC content (% GCO) of gene mRNA 
	
__2. Full\_Best5PerClass\_classified\_sample\_proteins.txt__

__3. Full\_Best5Species\_classified\_sample\_proteins.txt__

Both files are almost identical to output files 2 and 3 in previous section, respectively, with the addition of a single column: 
	
	29. Common_Name - Blast hit sequence description.

## 3. Taxonomically classify genes as native or foreign using blast results taxonomies

### Description

The script identify\_foreign\_genes.py uses the taxonomic and blast information generated and uses a series of layered selection criteria to identify the final taxonomic class of a blast hit with a high level of confidence. 

### Example

	identify_foreign_genes.py -v complete_classified_sample_proteins.txt -s Full_Best5Species_classified_sample_proteins.txt

### Parameters

Mandatory parameters are marked with an asterisk (\*).

 	 [--help         | -h ] - Show help message
	*[--parsed_blast | -b ] - Parsed Blast output
	*[--species      | -s ] - Best5Species output

### Output files

__1. complete\_classified\_sample\_proteins\_Report.txt__
	
	1.  Model             - ID of protein of interest
	2.  AIclass           - Alien index classification. If < -25 is metazoan, if >25 
	3.  percent60         - Taxonomic class of 60% or more species in blast hits. If none were found it would have a value of NA
	4.  percent80         - Taxonomic class of 80% or more species in blast hits. If none were found it would have a value of NA
	5.  CovClass60        - Taxonomic class if the best blast hit sequence cover over 60% of the query sequence, otherwise NA 
	6.  CovClass70        - Taxonomic class if the best blast hit sequence cover over 70% of the query sequence, otherwise NA 
	7.  3o4o5Class        - If 3 or more of the top 5 blast hits belong to a single taxonomic class it will have this value otherwise NA
	8.  4o5Class          - If 4 or more of the top 5 blast hits belong to a single taxonomic class it will have this value otherwise NA
	9.  CovLengthClass200 - Taxonomic class if the best blast hit sequence covers 200 or more nts of the query sequence
	10. evalueClass6      - Taxonomic class of best blast hit if its value is below 1e-6
	11. evalueClass10     - Taxonomic class of best blast hit if its value is below 1e-10
	12. FinalClass        - Final class determined through a combination of selection criteria (see below)
	13. FinalClassRank    - Ranking of final taxonomic class according to level of confidence (from 1 high to 4 low)
	14. Errors            -
	15. FinalClassError   - If no errors were found it will be NA. Otherwise it can indicate if several classes suitable for final classification were found, or if the gene model remains Unclassified 
	16. Outlier1          - Is True when the gene has an odd distribution of parameters (3 out of 5 best blast hits in a taxonomic class but less than 60% of hits in the same taxonomic class) 
	17. Outlier2          - Is True when the gene has an odd distribution of parameters (If the AI is metazoan and either 3 out of 5 best blast hits or 60% of hits are non-metazoan. Also if the AI is non-metazoan but either 3 out of 5 best blast hits or 60% of hits are metazoan. ) 
	18. Outlier3          - Is True when the gene has an odd distribution of parameters (If the best hit class is one class and 3 out of 5 best blast hits or 60% of blast hits do not belong to the same taxonomic class.) 

### Note

This scripts add an additional taxonic class Mixed (denoted by X) when a sequence is clearly foreign yet does not fall cleanly into a single taxonomic classification as described above (Bacteria, Archaea, etc).


## 4. Classify genes as Native, Foreign\_HGT, etc using taxonomic classification of genes in the same scaffold

### Description

The identify\_hgt\_events.pl uses the gene content of each genomic scaffold to identify genes that are likely to be horizontally transferred (Foreign\_HGT), as well as identify other foreign genes that might also be horizontally transferred but, due to the lack of adjacent Native genes cannot be confidently classified as such.

### Example

	identify_hgt_events.pl -b complete_classified_sample_proteins.txt -i complete_classified_sample_proteins_Report.txt -g sample_genome.fa -m sample_coordinates.bed -o Final_classification_sample_proteins.txt

### Parameters

Mandatory parameters are marked with an asterisk (\*).

	*[--input   | -i  ] - Tabular input from identify_foreign_genes.py
	*[--output  | -o  ] - Tabular output of genes classified by genome context
	*[--blastc  | -b  ] - Blast taxonomic classification (output file 1 of classify_taxonomy_blast_hits.pl)
	*[--models  | -m  ] - Model position on genome (bed 12 format)
	*[--genome  | -g  ] - Genome of interest in fasta format

### Output files

__1. Classification\_Summary\_Final\_classification\_sample\_proteins.txt__
	
	1.  Model           - Gene model identifier
	2.  Scaffold        - Gene model host scaffold
	3.  Final_Class     - Final taxonomic classification of gene of interest
	4.  Class_Rank      - Ranking of final taxonomic class according to level of confidence (from 1 high to 4 low)
	5.  Gene_Status     - Foreign, Native or Unclassified
	6.  Scaffold_Status - Final scaffold status as per its gene content. Possible values: 
		* Native - All its genes are metazoan or unclassified
		* OnlyForeign - All its genes are Foreign
		* HGT - Has at least one foreign gene 
		* HGT1 - Has at least one foreign gene that is adjacent to a native gene (not separated by gaps)
		* Ambiguous - Has only foreign and unclassified genes
	7.  ModelSize       - Size of gene model (nt)
	8.  Scaffold_Size   - Size of scaffold (nt)
	9.  Start           - Start position of gene model in scaffold
	11. End             - End position of gene model in scaffold
	12. Strand          - Strand of gene model in scaffold
	13. Introns         - Number of introns in gene model
	14. ModelGC         - GC content (%) of gene mRNA
	15. Transfer_Status - Final transfer status classifications. Possible values: 
		* Native         - Native gene
		* Foreign_HGT    - Horizontally transferred foreign gene
		* Foreign_FS     - Foreign gene on a putatively foreign scaffold
		* Foreign_AS     - Foreign gene on an ambiguous scaffold
		* Unclassifiable - Gene that could not be assigned a taxomic classification using current information in the database 

__2. Final\_classification\_sample\_proteins.txt__

This file contains a subset of the fields contained on output 1 (above). The descriptions are identical and thus have been omitted. 

	1.  Model          
	2.  Scaffold       
	3.  Start          
	4.  End            
	5.  Strand         
	6.  Introns        
	7.  Scaffold_Size  
	8.  Final_Class    
	9.  Class_Level   	
	10. Gene_Status    
	11. Scaffold_Status
	12. Transfer_Status 

__3. Scaffolds\_Classification\_Final\_classification\_sample\_proteins.txt__

	1. Scaffold         - Name of genome scaffold
	2. Scaffold_Status  - Final scaffold status as per its gene content. Possible values as describe for output file 1 (above).
	3. Foreign_Total    - Total number of foreign genes on scaffold 
	4. Native_Total     - Total number of native genes on scaffold
	5. Unclassified     - Total number of unclassified genes on scaffold
	6. Models_in_Contig - Gene model identifiers of all genes annotated on each scaffold

# Interpreting your results

The main output generated by HGTracker is the classification of genes into the Native, Foreign\_HGT, Foreign\_FS, Foreign\_AS and Unclassifiable categories found in the last field (Transfer\_Status) of file [Classification_Summary_Final_classification_sample_proteins.txt](https://bitbucket.org/lizfernandez/hgtracker/overview#markdown-header-output-files_4). The transfer status of each gene can be broadly interpreted as follows:

* __Native__ - Gene with clear support for classification as metazoan. 

* __Unclassifiable__ - Genes with not enough evidence to be confidently classified as being of either metazoan or foreign (non-metazoan) origin given the current information available in the database.

* __Foreign_HGT__ - Gene with clear support for being non-metazoan and hosted in a scaffold that also has Native genes. Additionally the scaffold status might denote if the Foreign and Native gene are contiguous (HGT1) with no gaps, or in the same scaffold separated by gaps (HGT). The taxonomy rank (from 1 to 4) can be also be used to provide some guidance into which genes are very high confidence candidates (rank 1) which might be useful for candidate selection for targeted experimental studies.

* __Foreign_FS__   - Genes with clear support for being non-metazoan and hosted in a scaffold where all genes have also been classified as foreign. There are two main interpretations for this genes, both of which warrant further characterization on a case to case basis: 

	1. The gene is in a scaffold with few genes and it likely constitutes a gene that lacks enough information to be classified as an HGT gene. 
	2. The entire scaffold belongs to a foreign genome. This hypothesis might be likely if a large scaffold has a reasonable number of genes with other clear characteristics of the gene donor kingdom (codon usage, protein domains/signals, GC%, number of introns, etc). 

* __Foreign_AS__   - Genes with clear support for being non-metazoan and hosted in a scaffold that has no native genes and a mix of unclassifiable and foreign genes. These genes might also constitute HGT events that failed to be classified as metazoan due to missing genomic neighbours that can clearly be identified as metazoan. This can be the case for lowly sampled taxa in the database that might not have enough support to be classified as metazoan.  

# Notes

* All genes in a genome should be analysed in bulk for the analysis to be successful as the pipeline uses the taxonomic annotations of adjacent genes to identify genes that have been horizontally transferred.
* All steps should be carried out in the same directory for the pipeline to work successfully. 

# Cite us! 

If you've found this pipeline useful please cite us:

# FAQ

* How do X ??

# Mailing list


# Credits

The HGTracker horizontal gene transfer detection pipeline was developed at the [Degnan Lab](http://www.degnanlabs.info/Degnan_Marine_Genomics/Home.html) at the University of Queensland.

Selene L. Fernandez-Valverde

Timothy Stephens

