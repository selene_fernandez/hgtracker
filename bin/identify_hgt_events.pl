#!/usr/bin/env perl

# identify_foreign_genes.pl

# Copyright (C) 2013 Selene L. Fernandez-Valverde. 

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# http://www.gnu.org/licenses/gpl-3.0.txt

use warnings;
use strict;
use FindBin qw($Bin);
use lib "$Bin/../extlib/lib/perl5";
use Data::Dumper;
use Getopt::Long qw( :config bundling );

use constant{
	CLASS =>0,
	CLASS_LEVEL =>1,
	CONTIG =>2,
	ALIEN =>3, #boolean true for foreign, false for metazoan and NA for unclassifieds/unclassifieds
	SCAFFOLD =>4,
	TSTART =>5,
	TEND =>6,
	STRAND =>7,
	NUM_INTRONS =>8,
	CONTIG_STATUS =>9,
	# Constants for Foreign native classification
	FOREIGN => 0,
	NATIVE => 1, 
	UNCLASSIFIED => 2,
};
	
my (@files, $script, $usage, %options, $classification, $models, $genome_fasta, $genome_bed, $blast_class, $output);

$script = ( split"/", $0 )[ -1 ];

$usage = qq(
$script by Selene L. Fernandez-Valverde, April 2013.

$script Identifies horizontal gene transfer (HGT) events leveraging on
the taxonomic classifications of adjacent genes on the same scaffold. 

Genes are classified as:

- Native         - Native gene
- Foreign_HGT    - Horizontally tranferred foreign gene
- Foreign_FS     - Foreign gene on a putatively foreign scaffold
- Foreign_AS     - Foreign gene on an ambiguous scaffold
- Unclassifiable - Gene that could not be assigned a taxomic classification using current information in the database

Usage: $script -b <Classified blast hits> -i <Taxomically classified genes> -g <Genome in fasta format> -m <Gene positions in bed format> -o <Output file>

Options:
	[--input    | -i  ] - Table of taxonomically classified genes from identify_foreign_genes.py
	[--blastc   | -b  ] - Blast taxonomic classification (output of classify_taxonomy_blast_hits.pl) 
	[--models   | -m  ] - Gene model position on genome (bed 12 format) 
	[--genome   | -g  ] - Genome of interest in fasta format
	[--output   | -o  ] - Tabular output of genes classified by genome context
Example:

$script -b complete_classified_sample_proteins.txt -i complete_classified_sample_proteins_Report.txt -g sample_genome.fa -m sample_coordinates.bed -o Final_classification_sample_proteins.txt

);

GetOptions(
	\%options,
	"input|i=s",
	"models|m=s",
	"blastc|b=s",
	"genome|g=s",
	"output|o=s",
	);

# Reading variables
$classification = $options{ "input" };
$blast_class = $options{ "blastc" };
$models = $options{ "models" };
$genome_fasta= $options{ "genome" };
$output = $options{ "output" };
print STDERR $usage and exit if not ($classification && $output);
print STDERR $usage and exit if not ($genome_fasta);
$genome_bed = $genome_fasta.".bed";

################################################### MAIN ################################################################

my ($classes, $model, %contigs, $contig, $foreign, $all_scaffolds, $scaffold, 
    $out_hd, $models_in_scaffold, $mods_in_scaffold, $output_2, $out2_hd,
    $ev_class, $cont, %printed, $merged_classification, $base_models, 
    $base_genome, $line, %scaffolds, $Contigs_bedfile, $contigs_hd, @lines, 
    $count,  $model_classification, $scaffold_classification, $final_classification, 
    $hgt1, $alien_models, $native_models, $unclassified_models, $scaffold_size_hash, @fields, 
    $joined_hd, $full_info_hd ) = ();

$base_models = `basename $models .bed`;
chomp($base_models);
$base_genome = `basename $genome_bed .bed`;
chomp($base_genome);

# Getting gap file
`faToTwoBit $genome_fasta /tmp/$genome_fasta.2bit`;
`twoBitInfo -nBed /tmp/$genome_fasta.2bit /tmp/$genome_fasta.gaps`;
`faSize -detailed $genome_fasta > /tmp/$genome_bed`;
# Put gene models in hash

#`cat $models | sort -k 4,4 > /tmp/S_$models`;

`cat $classification | grep [[:space:]]selfOnly[[:space:]] -v | cut -f 1,13 | tail -n+2 | sort -k 1,1 > /tmp/Summary_$classification`;

`cat $classification | grep [[:space:]]selfOnly[[:space:]] | cut -f 1 > Prot_self_blast_hits_only_$classification`;

# Complementing gap regions and naming 
my $Contigs_bed = `bedtools complement -i /tmp/$genome_fasta.gaps -g /tmp/$genome_bed | sed 's/Contig/Contig\t/g' | sort -nk 2,3 | sed 's/Contig\t/Contig/g'`;

$Contigs_bedfile = "/tmp/Contigs_".$base_genome.".bed";
open ($contigs_hd ,">$Contigs_bedfile");
@lines = (split /\n/,$Contigs_bed);
$count=0;
foreach $line (@lines){
	$count++;
	print $contigs_hd "$line\tContig_$count\n";
}

`overlapSelect -mergeOutput /tmp/Contigs_$base_genome.bed $models /tmp/$base_models\_OV_Contigs_$base_genome.bed`;

`cat /tmp/$base_models\_OV_Contigs_$base_genome.bed | cut -f 1-12,16 | sort -k 4,4 > /tmp/S_$base_models\_OV_Contigs_$base_genome.bed`;

# join merged information with class information
`join /tmp/Summary_$classification /tmp/S_$base_models\_OV_Contigs_$base_genome.bed -1 1 -2 4 | sed 's/ /\t/g' | cut -f 1-5,7,11,14 | sort -k 8 > /tmp/All_Contigs_$base_models\_Summary.txt`;

$merged_classification = "/tmp/All_Contigs_".$base_models."_Summary.txt";

$scaffold_size_hash=get_scaffold_sizes($genome_bed);

($classes,$models_in_scaffold) = read_tab_file($merged_classification); # Getting data for CONTIGs as well as duplicated (junction) gene models

# Counting number of alien and native genes 
foreach $model (keys(%{$classes})){
	$contig = $classes->{$model}->[ CONTIG ];
	$foreign = $classes->{$model}->[ ALIEN ];
	$scaffold = $classes->{$model}->[ SCAFFOLD ];

	$contigs{ $contig }{ 'SCAFFOLD' } = $scaffold; 

	if (!$contigs{$contig}){
		#Create entry for CONTIG in hash	
		$contigs{ $contig }{ $foreign }=1; 
	}
	else{
		$contigs{ $contig }{ $foreign }++;
	}
	$scaffolds{ $scaffold }{ $foreign }+=1; 
	if ($foreign eq 'FOREIGN' || $foreign eq 'NATIVE'){
		$scaffolds{ $scaffold }{ 'CONTIGS' }{ $contig }{ $foreign }+=1; 
	}
}

# Classify hgt events
($all_scaffolds,$hgt1 ) = classify_ltg_events(\%scaffolds); # Classifying putative HGT

open ($out_hd ,">$output");
$output_2 = "Scaffolds_Classification_".$output;
open ($out2_hd ,">$output_2");

# Printing HGT events header
print $out_hd "Model\tScaffold\tStart\tEnd\tStrand\tIntrons\tScaffold_Size\tFinal_Class\tClass_Level\tGene_Status\tScaffold_Status\tTransfer_Status\n";

print $out2_hd "Scaffold\tScaffold_Status\tForeign_Total\tNative_Total\tUnclassified\tModels_in_Contig\n";



foreach $model (sort(keys(%{$classes}))){
	$scaffold = $classes->{ $model }->[ SCAFFOLD ];

	$classes->{ $model }->[ CONTIG_STATUS ] = $all_scaffolds->{ $scaffold }->{ 'CLASS' };

	if (!$printed{$model}){
	# Getting final classification	
	$model_classification = $classes->{$model}->[ ALIEN ];
	$scaffold_classification = $classes->{$model}->[ CONTIG_STATUS ];
	if ( $model_classification eq 'FOREIGN' ){
		if ($scaffold_classification eq 'OnlyForeign'){
			$final_classification = 'Foreign_FS'
		}
		if ($scaffold_classification =~ 'HGT'){
			$final_classification = 'Foreign_HGT'; 
			if (grep $_ eq $classes->{$model}->[ CONTIG ], @{$hgt1}){
				$scaffold_classification = 'HGT1'; 
			}
		}
		if ($scaffold_classification eq 'Native'){
			print "Error!: Foreign gene in native Contig!\n";
			exit
		}
		elsif ($scaffold_classification eq 'Unclassified'){
			print "Error!: Foreign gene in unclassified Contig!\n";
			exit
		}
		elsif ($scaffold_classification eq 'Ambiguous'){
			$final_classification = 'Foreign_AS'
		}
	}
	if ( $model_classification eq 'NATIVE' ){
		if ($scaffold_classification eq 'OnlyForeign'){
			print "Error!: Native gene classified as contamination!\n";
			exit
		}
		elsif ($scaffold_classification =~ 'HGT'){
			$final_classification = 'Native'
		}
		elsif ($scaffold_classification eq 'Native'){
			$final_classification = 'Native'
		}
		elsif ($scaffold_classification eq 'Unclassified'){
			print "Error!: Native gene in unclassified scaffold!\n";
			exit
		}
		elsif ($scaffold_classification eq 'Ambiguous'){
			print "Error!: Native gene in ambiguous scaffold!\n";
			exit
		}
	}
	if ( $model_classification eq 'UNCLASSIFIED' ){
		$final_classification = 'Unclassifiable'
	}
		print $out_hd "$model\t$scaffold\t$classes->{$model}->[ TSTART ]\t$classes->{$model}->[ TEND ]\t$classes->{$model}->[ STRAND ]\t$classes->{$model}->[ NUM_INTRONS ]\t$scaffold_size_hash->{ $scaffold }\t$classes->{$model}->[ CLASS ]\t$classes->{$model}->[ CLASS_LEVEL ]\t$model_classification\t$scaffold_classification\t$final_classification\n";
	$printed{$model}=1;
	}
	else { print "Model $model was already printed\n"; }
}

foreach $scaffold (keys %$all_scaffolds){
	$ev_class = $all_scaffolds->{ $scaffold }->{ 'CLASS' };
	
	$mods_in_scaffold = join(',',@{$models_in_scaffold->{$scaffold}});

	if ($all_scaffolds->{$scaffold}->{ 'FOREIGN' }){	
		$alien_models = $all_scaffolds->{$scaffold}->{ 'FOREIGN' };
	}
	else {
		$alien_models = 0;
	}
	if ($all_scaffolds->{$scaffold}->{ 'NATIVE' }){	
		$native_models = $all_scaffolds->{$scaffold}->{ 'NATIVE' };
	}
	else {
		$native_models = 0;
	}
	if ($all_scaffolds->{$scaffold}->{ 'UNCLASSIFIED' }){	
		$unclassified_models = $all_scaffolds->{$scaffold}->{ 'UNCLASSIFIED' };
	}
	else {
		$unclassified_models = 0;
	}
	print $out2_hd "$scaffold\t$ev_class\t$alien_models\t$native_models\t$unclassified_models\t$mods_in_scaffold\n";

}

`cat $blast_class | sort -k 1,1 > /tmp/S_$blast_class`;
`cat $output | sort -k 1,1 > /tmp/S_$output`;
`join /tmp/S_$blast_class /tmp/S_$output | sort -rk 1 | grep Model -v > /tmp/Joined_$output`;


open ($joined_hd, "/tmp/Joined_$output");
open ($full_info_hd, ">Classification_Summary_$output");

print $full_info_hd "Model\tScaffold\tFinal_Class\tClass_Rank\tGene_Status\tScaffold_Status\tModelSize\tScaffold_Size\tStart\tEnd\tStrand\tIntrons\tModelGC\tTransfer_Status\n";

while (<$joined_hd>){
	chomp($_);
	$line = $_;
	 @fields=(split /\s/,$line);	
	print $full_info_hd "$fields[0]\t$fields[18]\t$fields[24]\t$fields[25]\t$fields[26]\t$fields[27]\t$fields[16]\t$fields[23]\t$fields[19]\t$fields[20]\t$fields[21]\t$fields[22]\t$fields[17]\t$fields[28]\n"; 
}

# Removing temporary files 

`rm /tmp/S_$output /tmp/S_$blast_class /tmp/Joined_$output /tmp/$genome_bed /tmp/$genome_fasta.2bit /tmp/$genome_fasta.gaps /tmp/Summary_$classification /tmp/Contigs_$base_genome.bed /tmp/$base_models\_OV_Contigs_$base_genome.bed /tmp/S_$base_models\_OV_Contigs_$base_genome.bed /tmp/All_Contigs_$base_models\_Summary.txt`;

################################################### SUBROUTINES #########################################################

sub read_tab_file
{
    my ($file) = @_;
    my ($file_hd, %entries, $line, $model, $composite_class, $contig, 
        $foreign, $class, $class_level, @data, $scaffold, $tStart, $tEnd,
	$strand, %models_in_scaffold, $num_exons, $num_introns) = ();

    open ($file_hd ,"$file");
    while (<$file_hd>){
	chomp($_);
	$line=$_;
	($model,$composite_class,$scaffold,$tStart,$tEnd,$strand,$num_exons,$contig) = (split /\t/,$line);
	if ($composite_class=~ /\w\d/){ # If composite class equals a letter and and a number, split it
		($class,$class_level) =(split //,$composite_class) ;
	}
	else{
		$class = $composite_class;
		$class_level = "NA";
	}
	if ($class eq "M"){		 #If the gene class is metazoan classify as native
		$foreign="NATIVE";
	}
	elsif ($class eq "NA" ){
		$foreign="UNCLASSIFIED"; #If the gene class is NA - Not assigned mark as unclassified
	}
	elsif ($class=~ /[ABFOPUVX]/){    #If the gene class is other than metazoan or NA mark as Foreign
		$foreign="FOREIGN";
	}
	if (!grep $_ eq $model, @{$models_in_scaffold{$scaffold}}){
		push(@{$models_in_scaffold{$scaffold}},$model);
	}
	$num_introns= $num_exons-1;
	@data = ($class,$class_level,$contig,$foreign,$scaffold,$tStart,$tEnd,$strand,$num_introns);
	@{$entries{$model}} = @data;
	@data=();
    }

    return ( \%entries, \%models_in_scaffold );
}

sub classify_ltg_events 
{
	my ($scaffolds) = @_;
	my ($event_class, $alien, $native, $unclassified, @hgt1,
	%entries, $class, $scaffold, $scaffolds_in_scaffold ) = ();

	foreach $scaffold (keys %{$scaffolds}){
		$alien=$native=$unclassified=0;

		# Counting number of foreign, native and unclassified genes in each scaffold
		if ($scaffolds->{$scaffold}->{'FOREIGN'}){
			$alien = $scaffolds->{$scaffold}->{'FOREIGN'}
		}
		if ($scaffolds->{$scaffold}->{'NATIVE'}){
			$native = $scaffolds->{$scaffold}->{'NATIVE'}
		}
		if ($scaffolds->{$scaffold}->{'UNCLASSIFIED'}){
			$unclassified = $scaffolds->{$scaffold}->{'UNCLASSIFIED'}
		}
		
		# Classifying 
		if ($alien > 0){
			if ($native == 0){
				if ($unclassified == 0){
					$class = "OnlyForeign"; # If there is only foreign genes
				}
				elsif ($unclassified > 0){
					$class = "Ambiguous";  # If there is only foreign and unclassified genes
				}
				else{
					die "Error\n";
				}
			}
			elsif ($native > 0){
				$class = "HGT"; # If there is alien and native genes 
				foreach $contig (keys %{$scaffolds->{ $scaffold }{ 'CONTIGS' }}){
					if (scalar(keys %{$scaffolds->{ $scaffold }->{ 'CONTIGS' }->{ $contig }}) == 2 ){
						push (@hgt1,$contig); # Marking contig if HGT1
					}
				}
			}
			else{
				die "Error\n";
			}
		}
		elsif ($alien == 0){
			if ($native == 0){
				if ($unclassified == 0){
					die "Error: No genes!\n";
				}
				elsif ($unclassified > 0){
					$class = "Unclassified"; # If there is only unclassified genes
				}
				else{
					die "Error\n";
				}
			}
			elsif ($native > 0){
				$class = "Native"; # If there is native genes and no foreign genes
			}
			else{
				die "Error\n";
			}
		}
		else{
			die "Error\n";
		}
		
		$scaffolds->{$scaffold}->{'CLASS'} = $class;
	}

	return ($scaffolds, \@hgt1);
}

sub get_scaffold_sizes
{
	my ($genome_bed) = @_;
	my ($line, $genome_bed_hd, $scaffold, $size, %scaffold_sizes) = ();
	
	open ($genome_bed_hd, "/tmp/$genome_bed");
	
	while(<$genome_bed_hd>){
		$line=$_;
		chomp($line);
		($scaffold,$size) = (split/\t/,$line);
		$scaffold_sizes{ $scaffold } = $size;
	}

	return (\%scaffold_sizes);
}
