#!/usr/bin/env perl

# annotate_blast_hits.pl

# Copyright (C) 2013 Selene L. Fernandez-Valverde. 

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# http://www.gnu.org/licenses/gpl-3.0.txt

use warnings;
use strict;
use FindBin qw($Bin);
use lib "$Bin/../extlib/lib/perl5";
use Data::Dumper;
use Getopt::Long qw( :config bundling );

use constant{
	#Constants for bed file data 
	CHROM => 0,
	TAG_START => 1,
	TAG_END => 2,
	TAG_ID => 3,
	SCORE => 4,
	TAG_STRAND => 5,
	TAG_LEN => 6,
	THICK_START => 7,
	THICK_END => 8, 
	RGB => 9, 
	BLOCK_COUNT => 10,  
	BLOCK_SIZES => 11, 
	BLOCK_STARTS => 12, 
	INTRONS => 13,
};
	
my (@files, $script, $usage, %options, $input, $fasta, $output );

$script = ( split"/", $0 )[ -1 ];

$usage = qq(
$script by Selene L. Fernandez-Valverde, January 2013.

$script expands the taxonomy annotation by adding NCBI sequence descriptions and GC content. 

NOTE: The script assummes input generated using the script classify_taxonomy_blast_hits.pl
from the HGTracker pipeline. All outputs should be in the same directory where the script is
executed.
 
Usage: $script [options] -i <classify_taxonomy_blast_hits output> -f <cDNAs in fasta format> -o <enhanced results>

Options:
	[--input      | -i ] - Output from classify_taxonomy_blast_hits
	[--fasta      | -f ] - Fasta file containing the cDNA of classified sequences 
	[--output     | -o ] - Enhanced results including %GC and gene annotations

Example:

$script -i classified_sample_proteins.txt -f sample_mRNA.fa -o complete_classified_sample_proteins.txt

);

GetOptions(
	\%options,
	"input|i=s",
	"fasta|f=s",
	"output|o=s",
	);

$input = $options{ "input" };
$fasta = $options{ "fasta" };
$output = $options{ "output" };

print STDERR $usage and exit if not ($input && $fasta);

######################## MAIN #########################

my ( $fasta_hash, $input_hash, $best5perclass, $best5species ) = ();

# Opening fasta file
$fasta_hash = get_fasta_hash( $fasta );
$input_hash = get_input_hash( $input );

print_parsed( $output, $fasta_hash, $input_hash );

$best5perclass= "Best5PerClass_".$input;
$best5species= "Best5Species_".$input;

annotate_taxonomy( $best5perclass );
annotate_taxonomy( $best5species );

############################################## SUBROUTINES ################################################

sub annotate_taxonomy
{
	my ($sequences) = @_;
	my ($named_sequences, $names_file_hd, $named_sequences_hd, %gi_hash, $count,
	   $fields, $joined_fields, $gi, $description, $line, $beheaded_seqs_hd, $command) = ();

	$named_sequences = "Full_" . $sequences;
	open ( $named_sequences_hd, ">$named_sequences" );
	`cat $sequences | cut -f 1 | grep Gi_Target -v > /tmp/temp_list_gis_$sequences`;
	$command = "blastdbcmd -target_only -db nr -entry_batch /tmp/temp_list_gis_$sequences -outfmt \"%g %t\" | sed \'s/ /\\t/\' > /tmp/Names_$sequences";
	`$command`; 
	print $named_sequences_hd "Gi_Target\tClass_id\tSuperkingdom\tKingdom\tPhylum\tClass\tOrder\tFamily\tGenus\tSpecies\tModelID\tQuery_len\tHit_id\tPercent_ident\tAlig_length\tMismatches\tGaps\tQuery_start\tQuery_end\tHit_start\tHit_end\tPercent+\tE-value\tBit_score\tScore\tRefSeqID\tSeqCoverage\t%SeqCoverage\tCommon_Name\n";

	`cat $sequences | grep Gi_Target -v > /tmp/beheaded_sequences`;

	open ($names_file_hd, "/tmp/Names_$sequences"); 
	while (<$names_file_hd>){
		$line=$_;
		($gi,$description) = split(/\t/,$line);
		$gi_hash{$gi}=$description; # making gi -> description hash
	}
	#print Dumper (%gi_hash);

	open ($beheaded_seqs_hd, "/tmp/beheaded_sequences"); 
	$count =0;
	while (<$beheaded_seqs_hd>){
		chomp($_);
		$count++;
		my @fields = split(/\t/,$_);
		if($gi_hash{$fields[0]}){
			$fields[28]=$gi_hash{$fields[0]};	# Making last field protein description
		}
		else{
			$fields[28]="NA\n";			# If protein was not found in blastdb make NA
		}
		my $joined_fields = join ("\t",@fields);
		
		print $named_sequences_hd "$joined_fields";
	}
	print "$count sequences processed in $sequences file\n";

	close ($named_sequences_hd);

	`rm /tmp/temp_list_gis_$sequences /tmp/beheaded_sequences /tmp/Names_$sequences`;
}

sub get_fasta_hash
{
	my ( $fastafile ) = @_;	
	my ( $Records, @records, $record, $fasta_hd, @entry, $size, $GC_content, 
	     $GC_bundle, @GC_entry, $gc_entry, $model, $gc, %fasta_hash, %entries ) = ();

	$Records = `cat $fastafile`;
	@records=( split /\>/, $Records);

	open ( $fasta_hd, "$fastafile" );
	$GC_bundle=`faCount $fastafile | sed '/#seq/d' | awk '{PERCENT=((\$4+\$5)/\$2)*100 ; print \$1, \$2, PERCENT}' | sed 's/ /\t/g'`;
	(@GC_entry) = (split /\n/, $GC_bundle);
	
	foreach $gc_entry (@GC_entry){
		($model,$size,$gc) = (split /\t/, $gc_entry);
		$fasta_hash{ $model } = $size."\t".$gc;
	}
	
	#print Dumper (%fasta_hash);	
	return(\%fasta_hash);
}

sub get_input_hash 
{	
	my ( $input ) = @_; 
	my ( $entry_hd, @entry, $record, @split_record, $name, $joined_entry, %entries, $input_hd ) = (); 
	
	open ($input_hd, "$input");
	while (<$input_hd>){	
	chomp $_;
	$record=$_;
	(@entry) = (split /\n/, $record);
	foreach $record (@entry){
		(@split_record) = (split /\t/, $record);
		$name = $split_record[ 0 ];
		if ($name eq "Model"){
			next;
		}
		shift (@split_record);
		$joined_entry = join ("\t",@split_record);
		$entries{$name} = $joined_entry;
		}
	}
    
	#print Dumper (%entries);
	return ( \%entries );
}

sub print_parsed
{
	my ($output, $fasta, $input) = @_;	
	my ($output_hd, $key) = ();
	
	open( $output_hd, ">$output" );	
	print $output_hd "Model\tAlienIndex\tBestHitClass\tSecondBestClass\tUsedSeqs\tTotalSpecies\tTotalHits\tBM:MSeqs:MPSeqs:MSp:MPSp:BMH_GI:BMH_eval:BMH_Bsc:BMSCov:BMPCov\tBB:BSeqs:BPSeqs:BSp:BPSp:BBH_GI:BBH_eval:BBH_Bsc:BBSCov:BBPCov\tBA:ASeqs:APSeqs:ASp:APSp:BAH_GI:BAH_eval:BAH_Bsc:BASCov:BAPCov\tBF:FSeqs:FPSeqs:FSp:FPSp:BFH_GI:BFH_eval:BFH_Bsc:BFSCov:BFPCov\tBP:PSeqs:PPSeqs:PSp:PPSp:BPH_GI:BPH_eval:BPH_Bsc:BPSCov:BPPCov\tBO:OSeqs:OPSeqs:OSp:OPSp:BOH_GI:BOH_eval:BOH_Bsc:BOSCov:BOPCov\tBV:VSeqs:VPSeqs:VSp:VPSp:BVH_GI:BVH_eval:BVH_Bsc:BVSCov:BVPCov\tBU:USeqs:UPSeqs:USp:UPSp:BUH_GI:BUH_eval:BUH_Bsc:BUSCov:BUPCov\tNative_models\tModelSize\tModelGC\n";
	foreach $key (keys(%{$input})){	
		print $output_hd "$key\t$input->{ $key }\t$fasta->{ $key }\n";

	}
	close( $output_hd );
}

