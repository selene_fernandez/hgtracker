#!/usr/bin/env perl

# configure_blast_database.pl

# Copyright (C) 2012 Selene L. Fernandez-Valverde. 

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# http://www.gnu.org/licenses/gpl-3.0.txt

use warnings;
use strict;
use FindBin qw($Bin);
use lib "$Bin/../extlib/lib/perl5";
use Data::Dumper;
use Bio::DB::Taxonomy;
use Bio::Taxonomy;
use Bio::LITE::Taxonomy::NCBI;
use Getopt::Long qw( :config bundling );
use Bio::LITE::Taxonomy::NCBI::Gi2taxid qw/new_dict/;
use Bio::LITE::Taxonomy::NCBI::Gi2taxid;

	
my (@files, $script, $usage, %options, $blast, $output, $native_txid, $tax_dir );

$script = ( split"/", $0 )[ -1 ];

$usage = qq(
$script by Selene L. Fernandez-Valverde, August 2012.

$script configures the NCBI's nr blast and taxonomy database and prepares them for queries by the HGTracker pipeline.

Options:
	[--path        | -p ]  - Path to taxonomy database.

Example:

$script -p /path/where/my/database/is

);

GetOptions(
	\%options,
	"path|p=s",
	);

# Reading variables
$native_txid = 9606;  # Native tax id
# Setting up database 
$tax_dir = $options{ "path" };
print STDERR "\nERROR: Not path to taxonomy database provided. Please provide the location to the database.\n $usage" and exit if not $tax_dir;

################################################### MAIN ################################################################

my ($db, $nodefile, $namesfile, $node, $dict, $native_species ) = ();

# Opening an instance of the taxonomy database
($nodefile,$namesfile) = ($tax_dir."/nodes.dmp",$tax_dir."/names.dmp");
$db = new Bio::DB::Taxonomy(	-source    => 'flatfile',
                               -nodesfile => $nodefile,
                               -namesfile => $namesfile,
                               -directory => $tax_dir);

# Setup dictionary if it has not been previously setup (deactivated by default) 
# Setting up gi dictionary
	new_dict (	in => $tax_dir."/gi_taxid_prot.dmp",
          		out => $tax_dir."/gi_taxid_prot.bin");

$dict = Bio::LITE::Taxonomy::NCBI::Gi2taxid->new(dict=>$tax_dir."/gi_taxid_prot.bin");

if ($dict){
	print "Sucessfully built gi to taxonomy database.\n";
}
	
# Getting native species name
$node = $db->get_Taxonomy_Node(-taxonid => $native_txid);
# <domain> <phylum> <class> <order> <family> <genus> <species> <strain>	

for ( 1..40 ) {
	if ($node){
		my $rank = $node->rank;
		if ($rank eq "species"){
			$native_species = $node->scientific_name;
		}
	}
}

if ($native_species eq "Homo sapiens"){
	print "Sucessfully built taxonomy database.\n";
}

