#!/usr/bin/env python

# Based on code by Donovan Parks

###############################################################################
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program. If not, see <http://www.gnu.org/licenses/>.     #
#                                                                             #
###############################################################################

# This script downloads:
# a) The formated nr database for blast (NCBI)
# b) The full NCBI taxonomy database 
# c) The gitaxid dump database 

import os
import sys
import gzip
import platform
import tarfile
import urllib
import re
import fileinput
from ftplib import FTP


# Write a file to disk obtained via FTP
class FtpWriter:
	def __init__(self, file):
		self.f = open(file, 'wb')
		self.count = 0

	def __call__(self, block):
		self.f.write(block)
		
		if self.count % 1000 == 0:
			print '.',
			sys.stdout.flush()
			
		self.count += 1
		
	def close(self):
		self.f.close()

# Getting list of files in nr database
def DownloadBlastDB(nrDatabaseRegex):
	ncbiFTP = 'ftp.ncbi.nih.gov'
	blastDBDir = '/blast/db'
	
	# Connect to NBCI's FTP site using an anonymous account
	print 'Connecting to NCBI FTP site (' + ncbiFTP + ')...'
	ftp = FTP(ncbiFTP)
	print ftp.login()
	print '\n'

	# Change to directory containing taxonomy files
	print 'Changing to directory ' + blastDBDir
	print ftp.cwd(blastDBDir)
	print '\n'

	# Download taxonomy files
	print 'Getting list of nr database files...'
	BlastDBfiles = []
	nrDBfiles = []
	ftp.retrlines('NLST', BlastDBfiles.append)
	print 'Downloading nr database files...'
	print '  It may take a few minutes to download these files.'
	for BlastDatabaseFile in BlastDBfiles:
		if re.match(nrDatabaseRegex,BlastDatabaseFile):
			DownloadBlastDBfile(BlastDatabaseFile)
			print 'Downloaded file ' + BlastDatabaseFile
			nrDBfiles.append(BlastDatabaseFile)	
		else:
			continue
	return nrDBfiles


# Download nr  database
def DownloadBlastDBfile(nrDatabaseFile):
	bDownload = True
	if os.path.exists('./' + nrDatabaseFile):
		bValidResponse = False
		while not bValidResponse:
			response = raw_input('NCBI nr database file ' + nrDatabaseFile + ' already exists. Would you like to download the latest version [Y/N]? ')
			if response[0] == 'Y' or response[0] == 'y':
				bDownload = True
				bValidResponse = True
			elif response[0] == 'N' or response[0] == 'n':
				bDownload = False
				bValidResponse = True
				
	if bDownload:
		ncbiFTP = 'ftp.ncbi.nih.gov'
		blastDBDir = '/blast/db'
		
		# Connect to NBCI's FTP site using an anonymous account
		print 'Connecting to NCBI FTP site (' + ncbiFTP + ')...'
		ftp = FTP(ncbiFTP)
		print ftp.login()
		print '\n'

		# Change to directory containing taxonomy files
		print 'Changing to directory ' + blastDBDir
		print ftp.cwd(blastDBDir)
		print '\n'

		# Download taxonomy files
		print 'Downloading ' + nrDatabaseFile 
		
		ftpWriter = FtpWriter(nrDatabaseFile)
		msg = ftp.retrbinary('RETR ' + nrDatabaseFile, ftpWriter, 32*1024*1024)
		print '\n'
		print msg
		ftpWriter.close()
		
		ftp.quit()

# Download NCBI taxonomy database
def DownloadTaxonomy(taxonomyDump):
	bDownload = True
	if os.path.exists('./' + taxonomyDump):
		bValidResponse = False
		while not bValidResponse:
			response = raw_input('NCBI taxonomy file ' + taxonomyDump + ' already exists. Would you like to download the latest version [Y/N]? ')
			if response[0] == 'Y' or response[0] == 'y':
				bDownload = True
				bValidResponse = True
			elif response[0] == 'N' or response[0] == 'n':
				bDownload = False
				bValidResponse = True
				
	if bDownload:
		ncbiFTP = 'ftp.ncbi.nih.gov'
		taxonomyDir = '/pub/taxonomy'
		
		# Connect to NBCI's FTP site using an anonymous account
		print 'Connecting to NCBI FTP site (' + ncbiFTP + ')...'
		ftp = FTP(ncbiFTP)
		print ftp.login()
		print '\n'

		# Change to directory containing taxonomy files
		print 'Changing to directory ' + taxonomyDir
		print ftp.cwd(taxonomyDir)
		print '\n'

		# Download taxonomy files
		print 'Downloading taxonomy database files...'
		print '  It may take a few minutes to download these files.'
		
		ftpWriter = FtpWriter(taxonomyDump)
		msg = ftp.retrbinary('RETR ' + taxonomyDump, ftpWriter, 32*1024*1024)
		print '\n'
		print msg
		ftpWriter.close()
		
		ftp.quit()

def DownloadGi2TaxId(gitaxidDump):
	bDownload = True
	if os.path.exists('./' + gitaxidDump):
		bValidResponse = False
		while not bValidResponse:
			response = raw_input('NCBI taxonomy file ' + gitaxidDump + ' already exists. Would you like to download the latest version [Y/N]? ')
			if response[0] == 'Y' or response[0] == 'y':
				bDownload = True
				bValidResponse = True
			elif response[0] == 'N' or response[0] == 'n':
				bDownload = False
				bValidResponse = True
				
	if bDownload:
		ncbiFTP = 'ftp.ncbi.nih.gov'
		taxonomyDir = '/pub/taxonomy'
		
		# Connect to NBCI's FTP site using an anonymous account
		print 'Connecting to NCBI FTP site (' + ncbiFTP + ')...'
		ftp = FTP(ncbiFTP)
		print ftp.login()
		print '\n'

		# Change to directory containing taxonomy files
		print 'Changing to directory ' + taxonomyDir
		print ftp.cwd(taxonomyDir)
		print '\n'

		# Download taxonomy files
		print 'Downloading taxonomy database files...'
		print '  It may take a few minutes to download these files.'
		
		ftpWriter = FtpWriter(gitaxidDump)
		msg = ftp.retrbinary('RETR ' + gitaxidDump, ftpWriter, 32*1024*1024)
		print '\n'
		print msg
		ftpWriter.close()
		
		ftp.quit()


# Decompress genome file
def DecompressBlastDB(genomeFile):
	tar = tarfile.open(genomeFile, 'r:gz')
	tar.extractall('./nr_database/')
	tar.close()
	
# Decompress taxonomy files
def DecompressTaxonomy(taxonomyDump):
	tar = tarfile.open(taxonomyDump, 'r:gz')
	tar.extractall('./taxonomy/')
	tar.close()

def DecompressGi2TaxId(gitaxidDump):
	os.system('gunzip -c ' + gitaxidDump + ' > ./taxonomy/gi_taxid_prot.dmp')
	
taxonomyDump = 'taxdump.tar.gz'
gitaxidDump = 'gi_taxid_prot.dmp.gz'
nrDatabaseRegexp = 'nr.\d\d.tar.gz$'

print 'HGTracker 1.0'
print ''
print 'This script is maintained by Selene L. Fernandez-Valverde (uqslizbe@uq.edu.au)'
print 'Based on code by Donovan Parks'
print 'Changes to the NCBI FTP site or NCBI file formats may break this script.'
print 'Please contact me if this script is broken and we will try to resolve the issue.'

print '\n'
print 'Downloading non redundant protein (nr) blast database from NCBI:'
nrDatabaseFiles = DownloadBlastDB(nrDatabaseRegexp)
print '\n----------------------------------------------------------\n'

print 'Decompressing nr blast database:'

for nrDatabaseFile in nrDatabaseFiles:
	print 'Decompressing ' + nrDatabaseFile
	DecompressBlastDB(nrDatabaseFile)
print '\n----------------------------------------------------------\n'

print 'Downloading NCBI taxonomy database:'
DownloadTaxonomy(taxonomyDump)
print '\n----------------------------------------------------------\n'

print 'Decompressing taxonomy files:'
DecompressTaxonomy(taxonomyDump)
print '\n----------------------------------------------------------\n'

print 'Downloading protein gi to taxonomy ID database:'
DownloadGi2TaxId(gitaxidDump)
print '\n----------------------------------------------------------\n'

print 'Decompressing gi to taxonomy ID database files:'
DecompressGi2TaxId(gitaxidDump)
print '\n----------------------------------------------------------\n'

print 'Installation complete (0 warnings, 0 errors). '
