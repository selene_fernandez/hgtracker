#!/usr/bin/env perl

# classify_taxonomy_blast_hits.pl

# Copyright (C) 2012 Selene L. Fernandez-Valverde. 

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# http://www.gnu.org/licenses/gpl-3.0.txt

use warnings;
use strict;
use FindBin qw($Bin);
use lib "$Bin/../extlib/lib/perl5";
use Data::Dumper;
use Bio::Seq;
use Bio::DB::Taxonomy;
use Bio::Taxon;
use Bio::Taxonomy;
use Bio::LITE::Taxonomy::NCBI;
use Getopt::Long qw( :config bundling );
use Bio::LITE::Taxonomy::NCBI::Gi2taxid qw/new_dict/;
use Bio::LITE::Taxonomy::NCBI::Gi2taxid;
use Set::IntSpan; # package to get coverage over a numeric interval

use constant{
	# Constants for tags data 
	GI =>0,
	TAX =>1,
	# Constants for raw blast data
	B_Q_ID => 0,
	B_Q_LEN => 1, 
	B_H_ID => 2,
	B_PERCENT_IDENT => 3,
	B_ALIG_LEN => 4,
	B_MISMATCHES => 5,
	B_GAPS => 6,
	B_Q_BEG => 7,
	B_Q_END => 8,
	B_H_BEG => 9,
	B_H_END => 10,
	B_POS_PERCENT => 11,
	B_EVALUE => 12,
	B_BIT_SCORE => 13,
	B_SCORE => 14,
	B_GI => 15,
	B_REF => 16,
	B_COVERAGE => 17,
	B_PER_COV => 18,
	#B_COMMON_NAME => 16,
	# Constants for taxonomy
	GI_T =>0,
	CLASS_ID=>1,
	SKINGDOM =>2,
	KINGDOM =>3,
	PHYLUM =>4,
	CLASS =>5,
	ORDER =>6,
	FAMILY =>7,
	GENUS =>8,
	SPECIES =>9,
	ID => 10,
	Q_LEN => 11,
        Q_ID => 12,
        PERCENT_IDENT => 13,
        ALIG_LEN => 14,
        MISMATCHES => 15,
        GAPS => 16,
        Q_BEG => 17,
        Q_END => 18,
        H_BEG => 19,
        H_END => 20,
        POS_PERCENT => 21,
        EVALUE => 22,
        BIT_SCORE => 23,
        SCORE => 24,
	REF_SEQ => 25,
	COVERAGE => 26,
	PER_COV => 27,
	#COMMON_NAME => 26,
};
	
my (@files, $script, $usage, %options, $blast, $output, $native_txid, $tax_dir );

$script = ( split"/", $0 )[ -1 ];

$usage = qq(
$script by Selene L. Fernandez-Valverde, August 2012.

$script mixes blast output with taxonomy annotation and calls a sequence as metazoan or foreign. 
It also provides the taxonomic annotation for the best blast hit and the 5 best sequences overall 
(BestSequences_<results suffix>) and the best 5 sequences in each of the following taxonomic 
classes (according to the NCBI taxonomy) (Best5PerClass_<results suffix>):

M - Metazoa
B - Bacteria
F - Fungi
A - Archaea
P - Plants
O - Other Eukaryotes 
V - Viruses 
U - Unknowns (have no kingdom or superkingdom) 

A taxonomic dictionary must be created prior to the first run to be able to classify the hits.

NOTE: The script assummes the usage of a descriptive form of blast output (has query indicators)
For more information visit: https://bitbucket.org/lizfernandez/hgtracker

Usage: $script [options] -i <BlastOutput> -o <AlienRegions>

Options:
	[--input       | -i  ] - Input blast output file (MUST have descriptive comments)
	[--output      | -o ]  - Desired output file 
	[--taxid       | -t ]  - NCBI Tax ID of organism of interest (native)
	[--dictionary  | -d ]  - Renew taxonomy dictionary. Dictionary must be where taxonomy DB is. 
	[--path        | -p ]  - Path to taxonomy database.

Example:

$script -i results_sample_proteins.blast -o classified_sample_proteins.txt -t 400682 -p ../taxonomy/ 

);

GetOptions(
	\%options,
	"input|i=s",
	"output|o=s",
	"taxid|t=s",
	"dictionary|d",
	"path|p=s",
	);

# Reading variables
$blast = $options{ "input" };
$output = $options{ "output" };
$native_txid = $options{ "taxid" };  # Native tax id
# Setting up database 
$tax_dir = $options{ "path" };
print STDERR "\nERROR: No blast input provided, please provide a blast input\n $usage" and exit if not $blast ;
print STDERR "\nERROR: No output file given. Please provide an output file\n$usage" and exit if not $output;
print STDERR "\nERROR: Not taxid provided for organism of interest. Please provide the taxid.\n $usage" and exit if not ($native_txid);
print STDERR "\nERROR: Not path to taxonomy database provided. Please provide the location to the database.\n $usage" and exit if not $tax_dir;

my @all_class_ids = ('M','B','A','F','P','O','V','U');
################################################### MAIN ################################################################

my ($db, $nodefile, $namesfile, $node, $line, $ID, $query, @hits, @data,
    $hits_num, $dict, $best_5_seqs, $hits_counter, %seen, @Uniq_Native_hits, $Native_hits, $best_class_hits, 
    $num_clean_hits, $clean_hits, $Total_species, @all_hits, $species, $native_species, $blast_version ) = ();

# Opening an instance of the taxonomy database
($nodefile,$namesfile) = ($tax_dir."/nodes.dmp",$tax_dir."/names.dmp");
$db = new Bio::DB::Taxonomy(	-source    => 'flatfile',
                               -nodesfile => $nodefile,
                               -namesfile => $namesfile,
                               -directory => $tax_dir);

# Setup dictionary if it has not been previously setup (deactivated by default) 
if ( $options{ "dictionary" } ){
# Setting up gi dictionary
	new_dict (	in => $tax_dir."/gi_taxid_prot.dmp",
          		out => $tax_dir."/gi_taxid_prot.bin");
} 

$dict = Bio::LITE::Taxonomy::NCBI::Gi2taxid->new(dict=>$tax_dir."/gi_taxid_prot.bin");
	
open ( my $OUT_BEST5SEQS, ">Best5PerClass_$output" ) || die "Can't create file Best5PerClass_$output\n $usage";
print $OUT_BEST5SEQS "Gi_Target\tClass_id\tSuperkingdom\tKingdom\tPhylum\tClass\tOrder\tFamily\tGenus\tSpecies\tModelID\tQuery_len\tHit_id\tPercent_ident\tAlig_length\tMismatches\tGaps\tQuery_start\tQuery_end\tHit_start\tHit_end\tPercent+\tE-value\tBit_score\tScore\tRefSeqID\tSeqCoverage\t%SeqCoverage\n";

open ( my $OUT_BEST5SPECIES, ">Best5Species_$output" ) || die "Can't create file Best5Species_$output\n $usage";
print $OUT_BEST5SPECIES "Gi_Target\tClass_id\tSuperkingdom\tKingdom\tPhylum\tClass\tOrder\tFamily\tGenus\tSpecies\tModelID\tQuery_len\tHit_id\tPercent_ident\tAlig_length\tMismatches\tGaps\tQuery_start\tQuery_end\tHit_start\tHit_end\tPercent+\tE-value\tBit_score\tScore\tRefSeqID\tSeqCoverage\t%SeqCoverage\n";

open ( my $OUT, ">$output" ) || die "Can't create file $output\n $usage";
print $OUT "Model\tAlienIndex\tBestHitClass\tSecondBestClass\tUsedSeqs\tTotalSpecies\tTotalHits\tBM:MSeqs:MPSeqs:MSp:MPSp:BMH_GI:BMH_eval:BMH_Bsc:BMSCov:BMPCov\tBB:BSeqs:BPSeqs:BSp:BPSp:BBH_GI:BBH_eval:BBH_Bsc:BBSCov:BBPCov\tBA:ASeqs:APSeqs:ASp:APSp:BAH_GI:BAH_eval:BAH_Bsc:BASCov:BAPCov\tBF:FSeqs:FPSeqs:FSp:FPSp:BFH_GI:BFH_eval:BFH_Bsc:BFSCov:BFPCov\tBP:PSeqs:PPSeqs:PSp:PPSp:BPH_GI:BPH_eval:BPH_Bsc:BPSCov:BPPCov\tBO:OSeqs:OPSeqs:OSp:OPSp:BOH_GI:BOH_eval:BOH_Bsc:BOSCov:BOPCov\tBV:VSeqs:VPSeqs:VSp:VPSp:BVH_GI:BVH_eval:BVH_Bsc:BVSCov:BVPCov\tBU:USeqs:UPSeqs:USp:UPSp:BUH_GI:BUH_eval:BUH_Bsc:BUSCov:BUPCov\tNative_models\n";

open (my $NOHITS, ">Prot_no_blast_hits_$output");

# Getting native species name
$node = $db->get_Taxonomy_Node(-taxonid => $native_txid);
# <domain> <phylum> <class> <order> <family> <genus> <species> <strain>	

for ( 1..40 ) {
	if ($node){
		my $rank = $node->rank;
		if ($rank eq "species"){
			$native_species = $node->scientific_name;
		}
	}
}

$blast_version= `head -n1 $blast` ; 

# Opening blast output file 
# Using naked block to define local parser
{
    	local $/ = $blast_version;
	open ( my $BLAST, "$blast" ) || die "Can't open file $blast\n $usage";


    	while ($line = <$BLAST>) {
      		chomp($line);
		@data = (split /#/,$line);
      		if ($data[4] && $data[1]){ # Splitting lines incoming from blast output
			$ID = 0;
			$query = $data[1];
			$query =~ s/ Query: *//;
			print "$query";
			$query =~ s/ //g;
			$query =~ s/\n//g;
			chomp($query); 
			@all_hits = (split /\n/,$data[4]); #cointains all the hits
			(@hits) = ( grep $_ !~  /BLAST\sprocessed\s\d+\squeries/, @all_hits ) ; # Removing last line of blast output
			if (!@hits){
				next; # If no hits were found move to next input sequence in blast
			}
			$hits_num = shift(@hits); # Take out the first line that cointains the number of hits
			$hits_num =~ s/hits found//;	# Remove the trailing characters
			$hits_num =~ s/ //g;
			
			($hits_counter, $Native_hits, $best_class_hits, $Total_species, $num_clean_hits, $species ) = parse_hits( $ID, $hits_num, \@hits, $dict, $db, $OUT_BEST5SEQS, $OUT_BEST5SPECIES, \@all_class_ids, $native_species); # Parse the hits and get them into data structures	
			
			%seen = ();
   			@Uniq_Native_hits = grep { ! $seen{ $_ }++ } @{$Native_hits}; # Getting uniq Native Species hits
			calculate_and_print($ID,$best_class_hits,\@Uniq_Native_hits,$num_clean_hits,$hits_num,$query,$OUT, $hits_counter, \@all_class_ids, $Total_species, $species); # Calculating alien index and percentage of species and sorting out final output 
		
		}
		else {
			if (@data){
				$query = $data[1];
				$query =~ s/ Query: *//;
				print $NOHITS "$query"; # Printing queries with no hits
			}
			next; 
		}
		$ID++;
    	}
}

################################################### SUBROUTINES ################################################################

sub parse_hits
{	
	# Subroutine by Selene L. Fernandez 2012
	# Parses a chunk of blast hits
	my ( $id, $num, $data, $gi_db, $tax_db, $OUT_BEST5SEQS, $OUT_BEST5SPECIES, $classes, $native_species ) = @_;
	my ( $hit, @hits, @blast_entries, @sorted_entries, %blast, %best_seqs, %best_seq_tax, $entries,
             $num_seqs, @blast_entry, $entry, $hit_taxonomy, $Best_hitAoA, $AoA, $coverage, @best_seq_ids, 
	     $gi, $common_name, $seq, @final_entries, $class_id, %num_seqs_class, @Native_hits, $species, 
	     %best_species, %num_hits, $num_clean_hits, %best_class_entries, @composite, $class, 
	     %best_per_class, $total_species, %best_5_per_class, @gis, $percent_cov, $percent_cov_round,
	     %composites, @best_5_species, %best_5_species_tax ) = (); 

	# initializing num_hits hash to 0
	foreach $class (@{$classes}){
		$num_hits{ $class } = 0; 
	}
	$total_species=0;	

	foreach $hit (@{$data}){ 		# Parsing all blast hits for that particular entry	
		chomp($hit);
		@blast_entry = (split/\t/,$hit);
		my ($temp, $gi, $db_ref, $ref) = (split/\|/,$blast_entry[ B_H_ID ]);
		# Getting reference number for pir and prf databases
		if ($db_ref eq "pir" || $db_ref eq "prf"){
			$ref = (split/\|/,$blast_entry[ B_H_ID ])[4];
			$ref = $ref."_".$db_ref;
		}
		$blast_entry[ B_GI ] = $gi;
		$blast_entry[ B_REF ] = $ref;
		push(@blast_entries, [ @blast_entry ] );
		#warn Dumper (@blast_entries);
		}
		# Sorting all hits for that model first by evalue and then by bitscore
		@sorted_entries = sort { $a->[ B_EVALUE ] <=>  $b->[ B_EVALUE ] || $b->[ B_BIT_SCORE ] <=>  $a->[ B_BIT_SCORE ] } @blast_entries; 
		my $count=0;
		foreach $entry (@sorted_entries){     # Looping through all sorted hits to separate in one hitsAoA per sequence
			$gi = $entry->[ B_GI ];
			if (!$blast{$gi}){
				$count++;              # Count new best sequence 
				$blast{ $gi } = [ $entry ] ; # Creating first has entry for that id with array
				$best_seqs{ $count } = $gi;
			}
			else {
				push (@{$blast{ $gi }}, $entry); # Pushing sequence result array onto existing hash
			}
		} 
		for(my $key=1, $num_seqs=0; $key <= scalar(keys(%best_seqs)); $key++){  # Loop through best sequences to identify 5 best
			$seq = $best_seqs{ $key };	
			$hit = $blast{ $seq };
			$gi = $hit->[0]->[ B_GI ];
			$entries = gi_to_taxonomy( $gi_db, $gi, $tax_db);
			#Moving cleanup parsed here	
			if ($entries->[$id][ SPECIES ] eq "synthetic construct"){ # Ignoring synthetic constructs
				next;
			}
			if ($entries->[ $id ][ SPECIES ] eq $native_species){ # Identifying hits to native species
				push (@Native_hits, $hit->[0]->[ B_REF ]);
				next;
			}
			$class_id = $entries->[ 0 ][ CLASS_ID ]; 
			$species = $entries->[ 0 ][ SPECIES ];

			if (!$best_species{ $class_id }{ $species }){
				$best_species{ $class_id }{ $species } = 1; # If this species does not exist add it to the hash
				$total_species++; 			    # Increment the number of species
				if ($total_species < 6){
					push(@best_5_species,$gi); 	    # Push first best 5 species into array
				}
			}
			else {
				$best_species{ $class_id }{ $species }++;   # Increment counter for further occurences of this species
			}
			# Counting how many species belong to each taxonomic class
			if ($class_id eq 'M'){
				$num_seqs_class{$class_id}++; 
				$num_seqs++;
			}
			elsif ($class_id eq 'A'){
				$num_seqs_class{$class_id}++;
				$num_seqs++;
			}
			elsif ($class_id eq 'B'){
				$num_seqs_class{$class_id}++;
				$num_seqs++;
			}
			elsif ($class_id eq 'F'){
				$num_seqs_class{$class_id}++;
				$num_seqs++;
			}
			elsif ($class_id eq 'P'){
				$num_seqs_class{$class_id}++;
				$num_seqs++;
			}
			elsif ($class_id eq 'O'){
				$num_seqs_class{$class_id}++;
				$num_seqs++;
				}
			elsif ($class_id eq 'V'){
				$num_seqs_class{$class_id}++;
				$num_seqs++;
				}
			elsif ($class_id eq 'U'){
				$num_seqs_class{$class_id}++;
				$num_seqs++;
				}

			if ($num_seqs_class{ $class_id } < 6 ){
				if ($num_seqs_class{ $class_id } == 1){
					$best_per_class{ $class_id } = $gi;
				}
				$hit_taxonomy = $entries->[ 0 ];
				$best_seq_tax{ $gi } = $hit_taxonomy ; # %best_seq_tax has the taxonomy for the best sequences
				push(@{$best_5_per_class{ $class_id }},$gi);
			}
			if ($total_species < 6){
				$hit_taxonomy = $entries->[ 0 ];
				$best_5_species_tax{ $gi } = $hit_taxonomy ; # %best_5_species_tax has the taxonomy for the best species
			}
		}

	# Getting best hit per class	
		foreach $class (@{$classes}){
			if ($best_5_per_class{ $class }){
			@gis = @{$best_5_per_class{ $class }};
				foreach $gi (@gis){
					$AoA = $blast{ $gi };
					$coverage = get_coverage($AoA); # Gets coverage using set mapping
					$Best_hitAoA = $AoA->[0]; # Gets first (best) hit for an ordered AoA of blast hits
					$Best_hitAoA->[ B_COVERAGE ] = $coverage;

					$percent_cov = ($coverage/$Best_hitAoA->[ B_Q_LEN ]*100);
					$percent_cov_round = sprintf("%.2f", $percent_cov);
					if ($percent_cov_round>100){ $percent_cov_round = sprintf("%.2f", 100); } 

					$Best_hitAoA->[ B_PER_COV ] = $percent_cov_round ;
				 	$Best_hitAoA->[ B_BIT_SCORE ] =~s / //;
					##warn Dumper ($Best_hitAoA);  #<STDIN>;
					# Push taxonomy, gi, common name, max evalue, max score , query coverage, max percent identity for 5 best seqs
					@composite =  (@{$best_seq_tax{ $gi }},@{$Best_hitAoA}); 
					splice(@composite, 25, 1); # Cleaning repeated gi field
					@composite  = map { defined $_ ? $_ : '.' } @composite; # Mapping to cleanup null values
					$line = join("\t",@composite);
					print $OUT_BEST5SEQS "$line\n"; # Printing best 5 sequences
					$composites{ $gi } = $line;
					if (($best_per_class{ $class }) && ($gi == $best_per_class{ $class })){
						$best_class_entries{ $class } = [ @composite ]; 
					}
					#print Dumper (%composites);
				}
			}
		}
		foreach $gi (@best_5_species){
			if ( $composites{ $gi } ) {
				print $OUT_BEST5SPECIES "$composites{ $gi }\n"; # Printing 5 best species
			}
			else{
				$AoA = $blast{ $gi };
				$coverage = get_coverage($AoA); # Gets coverage using set mapping
				$Best_hitAoA = $AoA->[0]; # Gets first (best) hit for an ordered AoA of blast hits
				$Best_hitAoA->[ B_COVERAGE ] = $coverage;

				$percent_cov = ($coverage/$Best_hitAoA->[ B_Q_LEN ]*100);
				$percent_cov_round = sprintf("%.2f", $percent_cov);
				if ($percent_cov_round>100){ $percent_cov_round = sprintf("%.2f", 100); } 

				$Best_hitAoA->[ B_PER_COV ] = $percent_cov_round ;
				$Best_hitAoA->[ B_BIT_SCORE ] =~s / //;
				# Push taxonomy, gi, common name, max evalue, max score , query coverage, max percent identity for 5 best seqs
				@composite =  (@{$best_5_species_tax{ $gi }},@{$Best_hitAoA}); 
				splice(@composite, 25, 1); # Cleaning repeated gi field
				@composite  = map { defined $_ ? $_ : '.' } @composite; # Mapping to cleanup null values
				$line = join("\t",@composite);
				$composites{ $gi } = $line;
				print $OUT_BEST5SPECIES "$line\n"; # Printing 5 best species
			}
		}
		
	return (\%num_seqs_class, \@Native_hits, \%best_class_entries, $total_species, $num_seqs, \%best_species );	
}

sub get_coverage
{
	# Subroutine by Selene L. Fernandez 2012
	# Calculates sequence coverage from a set of blast hits in AoA format 
	my ( $AoA ) = @_;
	my ( $hit_set, $flag, $hit, $q_start, $q_end, $coverage ) = (); 

	# Creating new set of integers	
	$hit_set=Set::IntSpan->new;   
	$flag=0;

	foreach $hit (@{$AoA}){	
		$q_start = $hit->[ B_Q_BEG ];
		$q_end = $hit->[ B_Q_END ];
		$hit_set += [[($q_start,$q_end)]]; 
	}
	
	$coverage= abs($hit_set->size); # Calculating coverage
	
	return ($coverage);
}

sub gi_to_taxonomy
{
	# Subroutine by Selene L. Fernandez 2012
	# Reads a blast file in custom format and turns it into 
	# into a searchable AoA structure
	my ( $dict, $gi, $db ) = @_;
	my ( $taxid, @entry, @entries, $node, $p, @founds, $rank ) = (); 
	

	# Getting taxonid from conversion database
	$taxid = $dict->get_taxid($gi);	
	#print "gi $gi tax $taxid\n";
	$node = $db->get_Taxonomy_Node(-taxonid => $taxid);
	$entry[ GI_T ] = $gi;
	
  	$p = $node;

  	for ( 1..40 ) {
   		if ($p){
     		my $rank = $p->rank;
     		if ($rank eq "species")
     		{
       			$entry[ SPECIES ] = $p->scientific_name;
     		}
     		if ($rank eq "genus")
     		{
       			$entry[ GENUS ] = $p->scientific_name;
     		}
     		if ($rank eq "family")
     		{
       			$entry[ FAMILY ] = $p->scientific_name;
     		}
     		if ($rank eq "order")
     		{
       			$entry[ ORDER ] = $p->scientific_name;
     		}
     		if ($rank eq "class")
     		{
       			$entry[ CLASS ] = $p->scientific_name;
     		}
     		if ($rank eq "phylum")
     		{
       			$entry[ PHYLUM ] = $p->scientific_name;
     		}
     		if ($rank eq "kingdom")
     		{
       			$entry[ KINGDOM ] = $p->scientific_name;
			if ($entry[ KINGDOM ] eq "Metazoa"){
				$entry[ CLASS_ID ] = "M";
			}
			elsif ($entry[ KINGDOM ] eq "Fungi"){
				$entry[ CLASS_ID ] = "F";
			}
			elsif ($entry[ KINGDOM ] eq "Viridiplantae"){
				$entry[ CLASS_ID ] = "P";
			}
    		}
     		if ($rank eq "superkingdom")
     		{
       			$entry[ SKINGDOM ] = $p->scientific_name;
			if ($entry[ SKINGDOM ] eq "Bacteria"){
				$entry[ CLASS_ID ] = "B";
			}
			elsif ($entry[ SKINGDOM ] eq "Archaea"){
				$entry[ CLASS_ID ] = "A";
			}
			elsif ($entry[ SKINGDOM ] eq "Viruses"){
				$entry[ CLASS_ID ] = "V";
			}
    		}

     		$p = $db->get_Taxonomy_Node(-taxonid => $p->parent_id);
    		}
  	}
	if (!$entry[ KINGDOM ] && !$entry[ SKINGDOM ]){ # Unknown class doesn't have kingdom or superkingdom annotation
		$entry[ CLASS_ID ] = "U";
	}
	if (!$entry[ CLASS_ID ]){
		$entry[ CLASS_ID ] = "O";
	}
	if (!$entry[ SPECIES ]){
		$entry[ SPECIES ] = ".";
	}
	push (@entries, [$entry[ GI_T ], $entry[ CLASS_ID ], $entry[ SKINGDOM ],$entry[ KINGDOM ],$entry[ PHYLUM ],$entry[ CLASS ],$entry[ ORDER ],$entry[ FAMILY ],$entry[ GENUS ],$entry[ SPECIES ]]);

	return (\@entries);	
}

sub calculate_and_print
{
	# Subroutine by Selene L. Fernandez 2012
	my ($ID,$best,$Native_hits,$num_clean_hits,$hits_num,$query,$OUT, $hit_counter, $all_class_ids, $total_species, $species) = @_;
	my ( @sorted_keys, $alien_index, $key1, $key2, $class, %cond_hit, $Native_hits_out, $percent,
	     $percent_seqs_round, $alien_index_round, $flag_a, $flag_m, $best_metazoan, $best_alien, $key,
	     $species_in_class, $percent_species, $percent_species_round ) = (); 
	
	@sorted_keys = sort { $best->{$a}->[ EVALUE ] <=>  $best->{$b}->[ EVALUE ] || $best->{$b}->[ BIT_SCORE ] <=> $best->{$a}->[ BIT_SCORE ] } keys %{$best}; # sorting hash by keys evalue and bit score
	
	# Calculating alien index	
	
	$flag_a=0;
	$flag_m=0;
	$key1 = $sorted_keys [0];
	$key2 = $sorted_keys [1];
	if (!$key1){
		$key1= "Native_Species";
	}
	if (!$key2){
		$key2= "NA";
	}
	foreach $key (@sorted_keys){
		if ($key eq 'M' && $flag_m == 0){
			$best_metazoan = $best->{'M'}[ EVALUE ]+1e-200;   # Identifying best metazoan hit
			$flag_m = 1;
			next;	
		}
		elsif ($key ne 'M' && $flag_a == 0){
			$best_alien = $best->{$key}[ EVALUE ]+1e-200;   # Identifying best alien hit
			$flag_a = 1;
			next;
		}
		elsif ($flag_a == 1 && $flag_m == 1) {
			last;
		}	
	}
	if (!$best_metazoan && $best_alien){
		$best_metazoan = 1+1e-200;
	}
	elsif ($best_metazoan && !$best_alien){
		$best_alien = 1+1e-200;
	}
	
	if ($best_alien && $best_metazoan ) { 	
		$alien_index = (log($best_metazoan))-(log($best_alien));   # Alien index calculation
		$alien_index_round = sprintf("%.2f", $alien_index);        # Rounding up alien index
	}
	elsif(!$best_alien && !$best_metazoan ){
		$alien_index_round = "NA" ;
	}

	foreach $class (@{$all_class_ids}){
		if ($best->{$class}){
			# structure of entry:
			# Class:Number:gi of bh:%i of clean seqs in class: Number of species in class: Gi of bh:eval of bh:bitscore of bh:Seq coverage:%seq coverage
			$species_in_class = scalar(keys(%{$species->{ $class }}));
			# Calculating hit percentage
			$percent = ($hit_counter->{$class}/$num_clean_hits)*100;
			$percent_seqs_round = sprintf("%.2f", $percent);
			# Calculating species percentage
			$percent_species = ($species_in_class/$total_species)*100;
			$percent_species_round = sprintf("%.2f", $percent_species);
			
$cond_hit{$class} = "B".$class.":".$hit_counter->{$class}.":".$percent_seqs_round.":".$species_in_class.":".$percent_species_round.":".$best->{$class}[ GI_T ].":".$best->{$class}[ EVALUE ].":".$best->{$class}[ BIT_SCORE ].":".$best->{$class}[ COVERAGE ].":".$best->{$class}[ PER_COV ];
		}
		else { $cond_hit{$class}="NA:NA:NA:NA:NA:NA:NA:NA:NA:NA";	}

	}
	
	# Condensing all Native IDS into one simple entry
	if (@{$Native_hits}){
		$Native_hits_out=join(",",@{$Native_hits});
		#print Dumper ($Native_hits_out);
	}
	else { $Native_hits_out = "NA";}
	
	# Printing condensed results to output
	print $OUT "$query\t$alien_index_round\t$key1\t$key2\t$num_clean_hits\t$total_species\t$hits_num\t$cond_hit{ 'M' }\t$cond_hit{ 'B' }\t$cond_hit{ 'A' }\t$cond_hit{ 'F' }\t$cond_hit{ 'P' }\t$cond_hit{ 'O' }\t$cond_hit{ 'V' }\t$cond_hit{ 'U' }\t$Native_hits_out\n" ;

}


########################################## END_OF_FILE #######################################################################
