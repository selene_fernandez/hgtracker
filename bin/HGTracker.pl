#!/usr/bin/env perl

# $Id: HGTracker.pl 

# Copyright (C) 2014 Selene L. Fernandez-Valverde. 
# Based on code by Brian Hass

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# http://www.gnu.org/licenses/gpl-3.0.txt

use warnings;
use strict;
use Data::Dumper;
use Getopt::Long qw( :config bundling );

my (@files, $script, $usage, %options, $proteins, $blast, $bedfile, $cdna, $genome,
    $sample_name, $taxonomy_id );

$script = ( split"/", $0 )[ -1 ];

$usage = qq(
$script by Selene L. Fernandez-Valverde, July 2014.

$script uses sequence similarity to identify the likely taxonomic origin of 
a sequence and identify it is of metazoan or foreign origin. Using this information
in addition to the taxonomic classification of neighboring genes in the genome it 
classifies each sequence as Native, Putative HGT, Putative Contamination, Ambiguous
or Unknown. 

NOTE: The script assummes the usage of a descriptive form of blast output (has query indicators)
This script must be run on the same nr database that was used to carry out the blast search

Usage: $script [options] -p <Protein fasta file> -b <Protein positions in genome> -c <mRNA sequences in DNA format> -g <Genome in fasta format> -r <Blast results>

Options:
	[--proteins  | -p ] - Protein file in fasta format
	[--bedfile   | -b ] - Protein coordinates in genome of interest in bed12 format
	[--cDNA      | -c ] - mRNA sequences in fasta format (cDNA)
	[--genome    | -g ] - Genome of interest in fasta format
	[--taxonomy  | -t ] - Taxonomy ID number for the organism of interest from the NCBI Taxonomy DB
	[--blast     | -r ] - Blast results for protein file (see manual for specs) [optional]

Example:

$script -p sample_proteins.fa -b sample_coordinates.bed -c sample_mRNA.fa -g sample_genome.fa -r results_sample_proteins.blast

);

GetOptions(
	\%options,
	"proteins|p=s",
	"blast|r=s",
	"bedfile|b=s",
	"cDNA|c=s",
	"taxonomy|t=i",
	"genome|g=s",
	);

$proteins = $options{ "proteins" };
$bedfile = $options{ "bedfile" };
$cdna = $options{ "cDNA" };
$taxonomy_id = $options{ "taxonomy" };
$genome = $options{ "genome" };

print STDERR $usage and exit if not ($proteins);
print STDERR $usage and exit if not ($bedfile);
print STDERR $usage and exit if not ($cdna);
print STDERR $usage and exit if not ($taxonomy_id);
print STDERR $usage and exit if not ($genome);

if ($proteins){
	$sample_name = (split/\./,$proteins)[0];
}

######################## MAIN #########################


main: {
  
  if ($options{ "blast" }) {
	$blast = $options{ "blast" };
  	goto classify_blast_hits;
  }
  
  run_blast:
	{
		print "********* Running blast ************\n";

		my $cmd = "blastp -query $proteins -evalue 0.0001 -max_target_seqs 200 -db nr -num_threads 8 -outfmt '7 qseqid qlen sseqid pident length mismatch gapopen qstart qend sstart send ppos evalue bitscore score' -out results_$sample_name.blast";
		$blast = "results_$sample_name.blast";
		&process_cmd($cmd);
	}

  classify_blast_hits:
	{
		print "********* Classifying blast hits ************\n";

		my $cmd = "classify_taxonomy_blast_hits.pl -i $blast -o classified_$sample_name.txt -t $taxonomy_id -p ../taxonomy/";
		&process_cmd($cmd);
	}


  annotating_blast_hits:
	{

		print "********* Annotating blast hits ************\n";

		my $cmd = "annotate_blast_hits.pl -i classified_$sample_name.txt -f $cdna -o complete_classified_$sample_name.txt";

		&process_cmd($cmd);
	}
  
  identify_foreign_genes:
	{

		print "********* Identifying foreign genes ************\n";

		my $cmd = "identify_foreign_genes.py -b complete_classified_$sample_name.txt -s Full_Best5Species_classified_$sample_name.txt";

		&process_cmd($cmd);
	}
  
  identify_hgt_events:
	{

		print "********* Identifying hgt events ************\n";

		my $cmd = "identify_hgt_events.pl -b complete_classified_$sample_name\.txt -i complete_classified_$sample_name\_Report.txt -g $genome -m $bedfile -o Final_classification\_$sample_name.txt";
		&process_cmd($cmd);
	}




	exit(0);
}

####
sub process_cmd {
    my ($cmd) = @_;

    print "CMD: $cmd\n\n";
    my $ret = system($cmd);
    if ($ret) {
        die "Error, cmd: $cmd died with ret($ret) ";
    }

    return;
}

