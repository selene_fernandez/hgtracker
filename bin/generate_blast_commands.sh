#!/usr/bin/env bash
# $Id: generate_blast_commands.sh s.fernandezvalverde $

# Copyright (C) 2013 Selene L. Fernandez-Valverde. 

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# http://www.gnu.org/licenses/gpl-3.0.txt

function usage() {

echo "generate_blast_commands.sh by Selene L. Fernandez-Valverde, February 2013."
echo ""
echo "generate_blast_commands.sh automates the blasting step prior to taxonomy classification. It outputs"
echo "a list of the blast commands that need to run before doing the automated classification"
echo "and it splits up the fasta input in order for the commands to be run in parallel."
echo ""
echo "Usage: generate_blast_commands [options] -f <Fasta> -n <Number of Sequences> -t <Number of threads> -p <Path to fasta file>"
echo ""
echo "Options:"
echo "	[ -f ] - Input fasta file containing peptides that will be blasted"
echo "	[ -n ] - Number of sequences in each file in which the fasta will be split "
echo "	[ -p ] - path of fasta and ouput file where blast will be done."
echo "	[ -t ] - number of threads per blast job (available CPUs) "
echo "            Useful when running blast in different computer/server."
echo ""
echo "Example:"
echo ""
echo "generate_blast_commands.sh -f sample_proteins.fa -n 100 -t 4 -p /home/user/analysis_directory/"
echo ""
exit 1
}

if [ -z "$1" ]
  then
    echo -e "\nERROR: Please supply arguments\n"
    usage
fi

# Getting command line options
while getopts ":hf:n:p:t:" opt 
do 
    case "$opt" in
  	h) usage ;;
  	f) fasta="$OPTARG" ;;
  	n) numseqs="$OPTARG" ;;
  	p) fasta_path="$OPTARG" ;;
  	t) num_threads="$OPTARG" ;;
  	\?) echo -e "\nERROR: Invalid option; -$OPTARG \n"; usage ; exit >&2 ;;
        :) echo -e "\nERROR: Missing option argument for -$OPTARG \n" ; usage ; exit >&2 ;;
    esac
done

if [ ! -f ${fasta} ];
then
	echo -e "\nERROR: File ${fasta} not found!\n"
	usage
fi

if [ -f Blast_Commands_${fasta}.txt ];
then
	echo -e "\nERROR: A set of blast commands for ${fasta} already exists\n in this directory, please delete and retry.\n"
	usage
fi

if ! [[ "${numseqs}" =~ ^[0-9]+$ ]]
then
	error; echo -e "\nERROR - no number of sequences provided: ${numseqs} is not a number\n"
	usage
	exit >&2
fi

if ! [[ "${num_threads}" =~ ^[0-9]+$ ]]
then
	error; echo -e "\nERROR - no number of threads provided: ${num_threads} is not a number\n"
	usage
	exit >&2
fi

######################## MAIN #########################

# Get fasta IDS
cat ${fasta} | grep '^>' | sed 's/>//g' > ${fasta}_IDS

# Split fasta file
split -a 3 -l ${numseqs} ${fasta}_IDS

# Generate list of blast commands
counter=0
for ids in x???
 	do 
	((counter++))
	faSomeRecords ${fasta} $ids ${fasta}_${counter}.faa
	wait
	echo blastp -query ${fasta_path}${fasta}_${counter}.faa -evalue 0.0001  -max_target_seqs 200 -db nr -num_threads ${num_threads} -outfmt "'7 qseqid qlen sseqid pident length mismatch gapopen qstart qend sstart send ppos evalue bitscore score'" -out ${fasta_path}${fasta}_$counter.blast >> Blast_Commands_${fasta}.txt
	wait
done
rm x???

