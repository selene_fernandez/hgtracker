#!/usr/bin/env python

# Copyright (C) 2013 Timothy Stephens and Selene L. Fernandez-Valverde and Simone Higgie 

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# http://www.gnu.org/licenses/gpl-3.0.txt

################################################### INPUT ############################################################

import sys
import argparse

############ USER INPUT ############

# Takes two string args from command line and adds them to a list. 
parser = argparse.ArgumentParser(description = '''Script that assigns the taxonomic class of a group of blast hits.
\n
Example:
\n
identify_foreign_genes.py -v complete_classified_sample_proteins.txt -s Full_Best5Species_classified_sample_proteins.txt 
''',
				 epilog='''For more information visit https://bitbucket.org/lizfernandez/hgtracker''')
parser.add_argument('-b','--parsed_blast', help='File with parsed blast output')
parser.add_argument('-s','--species', help='File of Best Species')


######################################### TEST INPUT #########################################
args = parser.parse_args()
######################################### TEST INPUT #########################################

# The first part of each file name which is conserved between all files of
# previous output. 
fileName1 = args.parsed_blast # Data
fileName2 = args.species  #4o5class

'''Input for main file'''
# Tries to open file given file name from input
# Else if file does not exist catches IOError and return error message then exits program
try:
    rawData1 = open(fileName1, 'r') # Open file read mode
except IOError:
    print fileName1, "does not exist"
    sys.exit(1)


'''Input for 4o5 data file'''
# Tries to open file given file name from input
# Else if file does not exist catches IOError and return error message then exits program
try:
    rawData2 = open(fileName2, 'r') # Open file read mode
except IOError:
    print fileName2, "does not exist"
    sys.exit(1)


############ INPORTED FILE DATA EXTRACTION AND MANAGEMENT ############
''' main file data extraction '''
dataList = []

# Put each row from file as a string into a list
for row in rawData1:
    dataList.append(row.rstrip('\n')) # Strips \n from end of each row

# If the file contains one row or is an empty file, an error message is returned and the program is exited
if len(dataList) <2:
    print "Not Enought Rows"
    sys.exit(1)

# Set first row as title row
headings1 = ()
headings1 = dataList[0]
# Takes variable of type string with tokens seperated by \t and :
# Replaces all \t with : then splits the string by :
headingsplit = headings1.replace('\t', ':').split( ':' ) 

# Create a list of dictonaries from data from file
# Split values from title will be used as keys for each dictionary
dataListDict = [] # List of Dictionares
for i, row in enumerate(dataList): # Iterates through with row = list value(string) and i = list index
    if (i > 0): # i value above index 0 as index zero maps to heading
        d = {}
        rowsplit = row.replace('\t', ':').split( ':' ) # Split row of type string into tokens
        
        # Iterates through with cell = each token value from that row and i = token index value.
        # Use i to pull out heading from headingsplit to use as key for its corrosponding token. 
        for i,cell in enumerate(rowsplit):
            d[headingsplit[i]] = cell # key:value
        dataListDict.append(d) # Add each dictionary to end of list

rawData1.close() # Close raw data file

''' 4o5 file data extraction '''
Data4o5 = []

# Put each row from file as a string into a list
for row in rawData2:
    Data4o5.append(row.rstrip('\n')) # Strips \n from end of each row

# If the file contains one row or is an empty file, an error message is returned and the program is exited
if len(Data4o5) <2:
    print "Not Enought Rows in 4o5"
    sys.exit(1)

# Set first row as title row
headings2 = ()
headings2 = Data4o5[0]
headingsplit = headings2.split( '\t' ) # Split by white space

# Create a list of dictonaries from data from file
# Split values from title will be used as keys for each dictionary
Data4o5List = [] # List of Dictionaries
for i, row in enumerate(Data4o5): # Iterates through with row = list value(string) and i = list index
    if (i > 0): # i value above index 0 as index zero maps to heading
        d = {}
        rowsplit = row.split('\t') # Split row into tokens
        
        # Iterates through with cell = each token value from that row and i = token index value.
        # Use i to pull out heading from headingsplit to use as key for its corrosponding token. 
        for i,cell in enumerate(rowsplit):
            d[headingsplit[i]] = cell # key:value
        Data4o5List.append(d) # Add each dictionary to end of list

rawData2.close() # Close raw data file



################################################### MAIN #############################################################

'''
# UsedSeqs: Number of used sequences, one value per sequence 
# AI: AlienIndex, one value per sequence 
# PSp: % of species found in this taxonomic class (MPSp, BPSp, APSp ... etc)
# Cov: % Coverage of blast query sequence by best hit in this class (BM.Cov, BB.Cov, BA.Cov, ... etc)
# FourOrFive: Last 8 columns of input, they provide a number from 1 to 5 indicating how many of the 5 best hits correspond to that particular taxonomic class (column headers are equal to taxonomic classes (A,B,M ... etc)
# CovLength: Number of nucleotides covered by best hit in this class (BMS_Cov, BBS_Cov, BAS_Cov ... etc)
# Evalue: E-value of best hit in this class (BMH_eval, BBH_eval, BAH_eval ... etc) 

# Classifications
'''
#Temperary title keys. Contains keys which are only used during anaysis and will not
# be passed as part of the output. 
tempTitleKeys = ['A', 'B', 'F', 'M', 'U', 'V', 'P', 'O', 'Model', 'AIclass', 'percent60', 'percent80', 'CovClass60', 'CovClass70', '3o4o5Class', '4o5Class', 'CovLengthClass200', 'evalueClass6', 'evalueClass10', 'FinalClass', 'FinalClassRank', 'Errors', 'FinalClassError', 'Outlier1', 'Outlier2', 'Outlier3']
# Keys to be printed in output. 
outputTitleKeys = ['Model', 'AIclass', 'percent60', 'percent80', 'CovClass60', 'CovClass70', '3o4o5Class', '4o5Class', 'CovLengthClass200', 'evalueClass6', 'evalueClass10', 'FinalClass', 'FinalClassRank', 'Errors', 'FinalClassError', 'Outlier1', 'Outlier2', 'Outlier3']
# Define letter abreviations for taxinomic classes
classes = ('A', 'B', 'F', 'M', 'U', 'V', 'P', 'O')

# Open try statement to catch if KeyError is presented.
try:
    # Set Model ID of first line in data set. 
    l = Data4o5List[0]
    lineName = ''
    lineName = l['ModelID']

    model = {}
    # For each taxclass set as key for dictionary with 0 as value
    for taxclass in classes:
        model[taxclass] = 0
    
    # List of Dictionaries containing each model with corresponding 4o5 values
    class4o5List = [] 
    # Dictionary containing model name as key with index of data in class4o5List as values.
    # This dictionary is used as a way of indexing all of the models as
    #   the two files many not be in sequential order.
    class4o5Dict = {} 

    # Iterates through each line in 4o5 data set.
    # Each Model ID will have associated Class_id's which are the top best hits.
    # NOTE: not all model ID's will be present in the 4o5 data. This is why
    #   the class Dictionary must be used as a index. 
    for line in Data4o5List:
        if lineName != line['ModelID']: # If new Model
            # If new model ID found
            class4o5List.append(dict(model)) # Append cound Dictionary to list
            class4o5Dict[lineName] = len(class4o5List) - 1 # Set Index of data in class4o5List
            lineName = line['ModelID']
            for taxclass in classes: # Reset count dictionary
                model[taxclass] = 0
            model[line['Class_id']] += 1
        else: # If old model
            model[line['Class_id']] += 1

    classLen = len(class4o5List) - 1
    #dataLisDict ==> Appended List of Dict entries from File
    #Reprot ==> List of Dict entries as result from analysis of dataLisDic
    rawDataSplit = fileName1.split('.') # Split file name from extension
    reportName = rawDataSplit[0] + '_Report.txt' # Add new file name + extension. 
    Report = open(reportName, 'w')

    
    # Write keys to file as headings
    Report.writelines(i+'\t' for i in outputTitleKeys)

    # For each row in unchecked data
    for dictEnt in dataListDict:

        lineDict = {}
        # Set titleKeys as keys for dictionary and 'NA' as defalt value.
        for headings in tempTitleKeys:
            lineDict[headings] = 'NA'
        
        lineDict['Model'] = dictEnt['Model'] # Update model ID
        
        # Asess AlienIndex and assign appropriate terms
        if (dictEnt['AlienIndex'] == 'NA'):
            lineDict['AIclass'] = "selfOnly"
        elif (float(dictEnt['AlienIndex']) <= -25):
            lineDict['AIclass'] = "Metazoan"
        elif (float(dictEnt['AlienIndex']) > -25 and float(dictEnt['AlienIndex']) < 50):
            lineDict['AIclass'] = "Ambiguous"
        elif (float(dictEnt['AlienIndex']) >= 50):
            lineDict['AIclass'] = dictEnt['BestHitClass']


        # Open flags for each vaiable where their is a limit
        # on the number of tax classes occuping large values.
        Clflag = 0
        Covflag = 0
        evalueflag = 0
        CovLenghtflag = 0
        fouror5flag = 0
        # Open a dictionary to contain relevant 4o5 class values
        set4o5 = {}
        
        # For each tax class perform the following:
        for taxclass in classes:
            
            '''
            The list class4o5List contains a list of dictionaries which contain the count for each
            individual taxinomic class based off of the data from the 4o5 data file.
            This statement takes the variable set4o5 which is the current line as defined by the index variable i4o5.
            If the ModelID matches that of the current line being iterated through from the dataListDict the values
            of each tax class are examined.
            The statement will
                ==>Assign tax class to variable if >3 hits in that class
                ==>Also assigns to variable if >4 hits
                ==>Throw Error if two classes have >3 hits
            
            If the ModelID is equal to the dataListDict ID then the i4o5 variable is iterated through one position
            so the next dictonary entery can be examined.
            If either ModelID != dataListDict ID or the values of each tax class do not satisfy any of the defined
            pearmeters then the value is left as 'NA'
            '''
            if (dictEnt['Model'] in class4o5Dict): # If modelID is a key in class4o5Dict
                Model = dictEnt['Model'] # Name of Model
                # Set variable to 4o5 class dictionary based off of index returned my the name of current model
                set4o5 = class4o5List[class4o5Dict[Model]] 
                if (int(set4o5[taxclass]) >= 3 and fouror5flag == 0):
                    fouror5flag = 1
                    lineDict['3o4o5Class'] = taxclass
                    if (int(set4o5[taxclass]) >= 4):
                        lineDict['4o5Class'] = taxclass
                # If two classes have >= 3 hits
                elif (int(set4o5[taxclass]) >= 3 and fouror5flag == 1):
                    # ERROR - two classes have fouror5 hits
                    lineDict['Errors'] = 'ERROR - two classes have fouror5 hits'
            
            '''
            if Total Species are > 5
            Assign tax class to variable if % of total species in tax class is >60%
            Also assigns to variable if >80%
            Throws if two classes have >60%
            '''
            if (dictEnt['TotalSpecies'] != 'NA' and dictEnt[taxclass + 'PSp'] != 'NA'):
                if (int(dictEnt['TotalSpecies']) > 5):
                    if (float(dictEnt[taxclass + 'PSp']) >= 60  and Clflag == 0):
                        Clflag = 1
                        lineDict['percent60'] = taxclass
                        if (float(dictEnt[taxclass + 'PSp']) >= 80):
                            lineDict['percent80'] = taxclass
                    elif (float(dictEnt[taxclass + 'PSp']) >= 60 and Clflag == 1):
                        #ERROR - two classes have 60 %
                        lineDict['Errors'] = 'ERROR - two classes have 60 %'
            
            '''
            If BestHitClass == taxclass
            Assign tax class to variable if % Coverage of blast query sequence by besthitclass >60%
            Also assigns to variable if >70%
            Throws if two classes have >60% 
            '''   
            if (dictEnt['BestHitClass'] == taxclass):
                if (dictEnt['B' + taxclass + 'PCov'] != 'NA'):
                    if (float(dictEnt['B' + taxclass + 'PCov']) >= 60 and Covflag == 0):
                        Covflag = 1
                        lineDict['CovClass60'] = taxclass
                        if (float(dictEnt['B' + taxclass + 'PCov']) >= 70):
                            lineDict['CovClass70'] = taxclass	
                    elif (float(dictEnt['B' + taxclass + 'PCov']) >= 60 and Covflag == 1):
                        # ERROR - two classes have 60 coverage
                        # This will not overwrite whenever this happens!
                        lineDict['Errors'] = 'ERROR - two classes have 60 coverage'
            
            '''
            Number of nucleotides covered by best hit in this class 
            Assign tax class to variable if >200 coverage in that class
            Throws if two classes have >200 coverage
            '''   
            if (dictEnt['BestHitClass'] == taxclass and dictEnt['B' + taxclass + 'SCov'] != 'NA'):
                if (int(dictEnt['B' + taxclass + 'SCov']) >= 200 and CovLenghtflag == 0):
                    CovLenghtflag = 1	
                    lineDict['CovLengthClass200'] = taxclass
                elif (int(dictEnt['B' + taxclass + 'SCov']) >= 200 and CovLenghtflag == 1):
                    #ERROR - two classes have 200 coverage # This will overwrite whenever this happens!
                    lineDict['Errors'] = 'ERROR - two classes have 200 coverage'
            
            '''
            E-value of best hit in this class 
            Assign tax class to variable if E <= 1.00E-6 or E <= 1.00E-10
            Throws if two classes have E <= 1.00E-6
            '''
            if (dictEnt['BestHitClass'] == taxclass and dictEnt['B' + taxclass + 'H_eval'] != 'NA'):
                if (float(dictEnt['B' + taxclass + 'H_eval']) <= float(1.00E-6) and evalueflag == 0):
                    evalueflag = 1
                    lineDict['evalueClass6'] = taxclass
                    if (float(dictEnt['B' + taxclass + 'H_eval']) <= float(1.00E-10)):
                        lineDict['evalueClass10'] = taxclass
                elif (float(dictEnt['B' + taxclass + 'H_eval']) <= float(1.00E-10)):
                    # ERROR - two classes have a low evalue!
                    lineDict['Errors'] = 'ERROR - two classes have a low evalue'
                

        
    ############################################### FINAL CLASSIFICATION #########################################################

        '''
        Final analysis of all newly defined variables in order to identify
        tax class with dominant trates.
        Results in final most likely tax class for each model.

        X != M but X == taxclass
        '''
        for taxclass in classes:
            
            # Classification M1
            if (taxclass == 'M' and lineDict['AIclass'] == 'Metazoan' and lineDict['percent80'] == taxclass and int(dictEnt['TotalSpecies']) >= 6 and lineDict['4o5Class'] == taxclass and lineDict['evalueClass10'] == taxclass
                and (lineDict['CovClass70'] == taxclass or lineDict['CovLengthClass200'] == taxclass) and dictEnt['BestHitClass'] == 'M'):
                lineDict[taxclass] = 1
            # Classification X1
            elif (taxclass != 'M' and lineDict['AIclass'] == taxclass and lineDict['percent80'] == taxclass and int(dictEnt['TotalSpecies']) >= 6 and lineDict['4o5Class'] == taxclass and lineDict['evalueClass10'] == taxclass
                and (lineDict['CovClass70'] == taxclass or lineDict['CovLengthClass200'] == taxclass) and dictEnt['BestHitClass'] == taxclass):
                  lineDict[taxclass] = 1

            # Classification M2 & X2
            elif (lineDict['AIclass'] == 'Ambiguous' and lineDict['percent80'] == taxclass and int(dictEnt['TotalSpecies']) >= 6 and lineDict['4o5Class'] == taxclass and lineDict['evalueClass10'] == taxclass
                and (lineDict['CovClass70'] == taxclass or lineDict['CovLengthClass200'] == taxclass) and dictEnt['BestHitClass'] == taxclass):
                  lineDict[taxclass] = 2

            # Classification M3 & X3
            elif (lineDict['percent80'] == taxclass and int(dictEnt['TotalSpecies']) >= 6 and lineDict['4o5Class'] == taxclass and dictEnt['BestHitClass'] == taxclass):
                  lineDict[taxclass] = 3


            # Classification M4 & X4
            elif (lineDict['percent60'] == taxclass and int(dictEnt['TotalSpecies']) >= 6 and lineDict['3o4o5Class'] == taxclass and lineDict['evalueClass6'] == taxclass
                  and (lineDict['CovClass60'] == taxclass or lineDict['CovLengthClass200'] == taxclass) and dictEnt['BestHitClass'] == taxclass):
                  lineDict[taxclass] = 4
	

        # A list of each possible Ranks to be iterated through. 
        CatRange = ['1', '2', '3', '4', '5']
        stopper = 0
        '''
        Iterates through each Rank in CatRange in sequential order, if a match for that Rank is associated with a
        taxclass that tax class is assigned along with the Rank to the models FinalClassRank. If two or more taxclasses
        have the same Rank an error is returned showing both taxclasses identified.
        '''
        for Number in CatRange:

            
            if (lineDict['AIclass'] == 'Native_Species'):
                lineDict['FinalClassRank'] = 'Native_Species'
                break

            # ID tax class
            for taxclass in classes:
                if (lineDict[taxclass] != 'NA'):
                    if (int(lineDict[taxclass]) == int(Number) and lineDict['FinalClassRank'] == 'NA'):
                        lineDict['FinalClass'] = taxclass
                        lineDict['FinalClassRank'] = taxclass + Number
                        stopper = 1
                    elif (int(lineDict[taxclass]) == int(Number) and lineDict['FinalClassError'] != 'NA'):
                        temp = lineDict['FinalClassError']
                        lineDict['FinalClassError'] = temp + ", " + taxclass, Number
                    elif (int(lineDict[taxclass]) == int(Number) and lineDict['FinalClassRank'] != 'NA'):
                        lineDict['FinalClassError'] = 'ERROR - Multiple same ranked classes for: ' + lineDict['FinalClassRank'] + ", " + taxclass + Number
            if (stopper == 1):
                break
            
        # If no Final Class Rank has been try to identify conflicting cases of HGT (two distinct classes) 
        if (lineDict['FinalClassRank'] == 'NA'):
            
            for taxclass in classes:
                # Classification X1
                if (taxclass != 'M' 
	            and lineDict['AIclass'] != 'M'
	            and lineDict['AIclass'] != 'NA'
		    and lineDict['percent80'] != 'M' 
		    and lineDict['percent80'] != 'NA'
		    and int(dictEnt['TotalSpecies']) >= 6 
		    and lineDict['4o5Class'] != 'M' 
		    and lineDict['4o5Class'] != 'NA' 
		    and lineDict['evalueClass10'] != 'M'
		    and lineDict['evalueClass10'] != 'NA'
                    and ((lineDict['CovClass70'] != 'M' and lineDict['CovClass70'] != 'NA') or (lineDict['CovLengthClass200'] != 'M' and lineDict['CovLengthClass200'] != 'NA' )) 
		    and dictEnt['BestHitClass'] != 'M'
		    and dictEnt['BestHitClass'] != 'NA'):
                      lineDict['FinalClass'] = 'X'
                      lineDict['FinalClassRank'] = 'X' + '1'
    
                # Classification M2 & X2
                elif (lineDict['AIclass'] == 'Ambiguous' 
                    and lineDict['percent80'] != 'M' 
                    and lineDict['percent80'] != 'NA' 
		    and int(dictEnt['TotalSpecies']) >= 6 
		    and lineDict['4o5Class'] != 'M' 
		    and lineDict['4o5Class'] != 'NA' 
		    and lineDict['evalueClass10'] != 'M'
		    and lineDict['evalueClass10'] != 'NA'
                    and ((lineDict['CovClass70'] != 'M' and lineDict['CovClass70'] != 'NA') or (lineDict['CovLengthClass200'] != 'M' and lineDict['CovLengthClass200'] != 'NA')) 
		    and dictEnt['BestHitClass'] != 'M'
		    and dictEnt['BestHitClass'] != 'NA'):
                      lineDict['FinalClass'] = 'X'
                      lineDict['FinalClassRank'] = 'X' + '2'
    
                # Classification M3 & X3
                elif (lineDict['percent80'] != 'M' 
                    and lineDict['percent80'] != 'NA' 
		    and int(dictEnt['TotalSpecies']) >= 6 
		    and lineDict['4o5Class'] != 'M' 
		    and lineDict['4o5Class'] != 'NA' 
		    and dictEnt['BestHitClass'] != 'M'
		    and dictEnt['BestHitClass'] != 'NA'):
                      lineDict['FinalClass'] = 'X'
                      lineDict['FinalClassRank'] = 'X' + '3'
    
                # Classification M4 & X4
                elif (lineDict['percent60'] != 'M' 
                    and lineDict['percent60'] != 'NA' 
		    and int(dictEnt['TotalSpecies']) >= 6 
		    and lineDict['3o4o5Class'] != 'M' 
		    and lineDict['3o4o5Class'] != 'NA' 
		    and lineDict['evalueClass6'] != 'M'
		    and lineDict['evalueClass6'] != 'NA'
                    and ((lineDict['CovClass60'] != 'M' and lineDict['CovClass60'] != 'NA') or (lineDict['CovLengthClass200'] != 'M' and lineDict['CovLengthClass200'] != 'NA')) 
		    and dictEnt['BestHitClass'] != 'M'
		    and dictEnt['BestHitClass'] != 'NA'):
                      lineDict['FinalClass'] = 'X'
                      lineDict['FinalClassRank'] = 'X' + '4'

		# Passing on information for SelfOnly hits
		elif (lineDict['AIclass'] == 'selfOnly'):
                      lineDict['FinalClass'] = 'SO'
                      lineDict['FinalClassRank'] = 'SO'

        # If no Final Class Rank has been determined return an error in the error column
                else:
                    lineDict['FinalClassError'] = 'Unclassified'
        
        
        #'Outlier1', 'Outlier2', 'Outlier3'
        for taxclass in classes:
            if (lineDict['3o4o5Class'] == taxclass and lineDict['percent60'] != taxclass and lineDict['percent60'] != 'NA' ):
                lineDict['Outlier1'] = 'True' 
                
            if (lineDict['AIclass'] == 'Metazoan' and ((lineDict['3o4o5Class'] != 'M' and lineDict['3o4o5Class'] != 'NA') or (lineDict['percent60'] != 'M' and lineDict['percent60'] != 'NA'))):
                lineDict['Outlier2'] = 'True' 
                    
            elif (lineDict['AIclass'] == taxclass and (lineDict['3o4o5Class'] == 'M' or lineDict['percent60'] == 'M')):
                lineDict['Outlier2'] = 'True' 
                
            if (dictEnt['BestHitClass'] == taxclass and ((lineDict['3o4o5Class'] != taxclass and lineDict['3o4o5Class'] != 'NA') or (lineDict['percent60'] != taxclass and lineDict['percent60'] != 'NA'))):
                lineDict['Outlier3'] = 'True' 

        
        values =[] # Temp list
        # Take a list if dictionary values seperated from their keys but still in logical order
        for heading in outputTitleKeys:
            values.append(str(lineDict[heading]))
        Report.writelines('\n') # New line of output
        Report.writelines(i+'\t' for i in values) # Write each list value to file tab delimited
    Report.close() # Close report file

# If at any point throughout the program it calls for a key in a dictionary which does not exist
# An error message is returned with the name of the key missing and the program exits. 
except KeyError, e:
    print "Column", e, "missing from data"
    sys.exit(1)
    
